package com.cdc.irimia.uaic_ez.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cdc.irimia.uaic_ez.Common.Constants;
import com.cdc.irimia.uaic_ez.Common.HelpMethods;
import com.cdc.irimia.uaic_ez.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity {

    String TAG = "TAG ";
    private Context context;
    private FirebaseAuth mAuth;
    private CheckBox rememberMe;

    private void setNavBarAndStatusBarColors(Activity activity) {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.setNavigationBarColor(getResources().getColor(R.color.uaicBlue));
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(activity, R.color.uaicBlue));
        }
    }

    private void writeSharedPrefs(String key, String value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(key, value).apply();
    }

    private String readSharedPrefs(String key) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(key, null);
    }

    private void checkRememberMe(EditText emailBox, EditText passBox, CheckBox checkBox) {
        String email = readSharedPrefs("remEmail");
        String pass = readSharedPrefs("remPass");
        String cb = readSharedPrefs("remCB");
        if (email == null || pass == null || cb == null) {
            return;
        }
        emailBox.setText(email);
        passBox.setText(pass);
        checkBox.setChecked(true);
    }

    private void doRememberMe(String email, String password) {
        writeSharedPrefs("remEmail", email);
        writeSharedPrefs("remPass", password);
        writeSharedPrefs("remCB", "yes");
    }

    private void resetPrefs() {
        writeSharedPrefs("remEmail", null);
        writeSharedPrefs("remPass", null);
        writeSharedPrefs("remCB", null);
    }

    private void firebaseSignIn(String email, String password, boolean isPrivate) {

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            boolean emailVerified = user.isEmailVerified();
                            if (emailVerified) {

                                if (rememberMe.isChecked()) {
                                    doRememberMe(email, password);
                                    downloadUser(user.getEmail(), isPrivate);
                                } else {
                                    resetPrefs();
                                }

                            }

                        } else {
                            Toast.makeText(context, "Authentication failed.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void downloadUser(String id, boolean isPrivate) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("users").document(id)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            if (document.exists()) {
                                Constants.setUser(document.getData());
                                String userGroup = Constants.actualUser.get("group").toString();
                                List<String> facultyNewsList = new ArrayList<>();

                                db.collection("faculties").document(Constants.actualUser.get("faculty").toString()).collection("news").get().addOnCompleteListener(
                                        new OnCompleteListener<QuerySnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                if (task.isSuccessful()) {
                                                    if (!task.getResult().isEmpty()) {
                                                        for (DocumentSnapshot documentSnapshot : task.getResult().getDocuments()) {
                                                            if (documentSnapshot.getData().containsKey("only_group")) {
                                                                if (documentSnapshot.getData().get("only_group").equals(userGroup)) {
                                                                    facultyNewsList.add(documentSnapshot.getId());
                                                                }
                                                            } else if (documentSnapshot.getData().containsKey("only_year")) {
                                                                if (documentSnapshot.getData().get("only_year").equals(userGroup.substring(0, 2))) {
                                                                    facultyNewsList.add(documentSnapshot.getId());
                                                                }
                                                            } else if (documentSnapshot.getData().containsKey("only_semi_year")) {
                                                                if (documentSnapshot.getData().get("only_semi_year").equals(userGroup.substring(0, 3))) {
                                                                    facultyNewsList.add(documentSnapshot.getId());
                                                                }
                                                            } else {
                                                                facultyNewsList.add(documentSnapshot.getId());
                                                            }
                                                        }
                                                    }
                                                }

                                                db.collection("users").document(id).collection("news")
                                                        .get()
                                                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                                if (task.isSuccessful()) {
                                                                    for (DocumentSnapshot document : task.getResult()) {
                                                                        facultyNewsList.remove(document.getId());
                                                                        if (!document.getData().containsKey("deleted")) {
                                                                            Constants.addNews(document.getId(), document.getData());
                                                                        }

                                                                    }

                                                                } else {
                                                                    Toast.makeText(context, "There was a problem trying to get data about you, please try again ...", Toast.LENGTH_LONG).show();
                                                                }

                                                                if (!facultyNewsList.isEmpty()) {
                                                                    for (String newsId : facultyNewsList) {
                                                                        db.collection("faculties").document(Constants.actualUser.get("faculty").toString()).collection("news").document(newsId).get().addOnCompleteListener(
                                                                                new OnCompleteListener<DocumentSnapshot>() {
                                                                                    @Override
                                                                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                                                        if (task.isSuccessful()) {
                                                                                            if (task.getResult().exists()) {
                                                                                                Constants.addNews(newsId, task.getResult().getData());
                                                                                                db.collection("users").document(id).collection("news").document(newsId).set(task.getResult().getData());
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                        );

                                                                    }
                                                                }
                                                                Constants.newsList = HelpMethods.newsSorter(Constants.newsList);
                                                                startMainActivity(isPrivate);
                                                            }
                                                        });

                                            }
                                        }
                                );


                            }

                        } else {
                            Toast.makeText(context, "There was a problem trying to get data about you, please try again ...", Toast.LENGTH_LONG).show();
                        }

                    }
                });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        askForNet();
        setContentView(R.layout.activity_login);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setNavBarAndStatusBarColors(this);
        final EditText email = findViewById(R.id.editText);
        final EditText password = findViewById(R.id.editText2);
        final TextView registerText = findViewById(R.id.registerText);
        final TextView guestLogIn = findViewById(R.id.guestLogIn);
        final Button loginButton = findViewById(R.id.button);

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();

        if (currentUser != null && !currentUser.getEmail().equals("cirimia@bitdefender.com")) {

            boolean emailVerified = currentUser.isEmailVerified();
            if (emailVerified)
                downloadUser(currentUser.getEmail(), false);
            else {
                email.setText(currentUser.getEmail().toString());
                password.setText("password");
                loginButton.setText("I verified my account");
                currentUser.sendEmailVerification()
                        .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(context, "Please check your email", Toast.LENGTH_LONG).show();

                                } else {
                                    Toast.makeText(context, "We cannot create an account, try again later.", Toast.LENGTH_LONG).show();
                                }
                            }
                        });
            }
        }


        guestLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startPrivateMainActivity();
            }
        });


        rememberMe = findViewById(R.id.rememberMeBox);

        checkRememberMe(email, password, rememberMe);


        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    String emailText = email.getText().toString();
                    String passText = password.getText().toString();
                    firebaseSignIn(emailText, passText, false);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        });

        registerText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startRegisterActivity();
            }
        });

    }

    private void startMainActivity(boolean isPrivate) {
        Intent intent = new Intent(this, MainActivity.class);
        if (isPrivate)
            intent.putExtra("private", true);

        startActivity(intent);
        this.finish();
    }


    private void startPrivateMainActivity() {
        mAuth.signOut();
        mAuth.signInWithEmailAndPassword("cirimia@bitdefender.com", "Password123")
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            downloadUser("cirimia@bitdefender.com", true);

                        } else {
                            Toast.makeText(context, "Authentication failed.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });


    }

    private void startRegisterActivity() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    public void askForNet() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    while (true) {
                        try {
                            if (!HelpMethods.isInternetAv(context)) {
                                LoginActivity.this.runOnUiThread(new Runnable() {
                                    public void run() {
                                        Toast.makeText(context, "Our app need an internet connection to work properly. Trying to reconnect ...", Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            Thread.sleep(500);
                        } catch (Exception e) {
                        }
                    }
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }


}
