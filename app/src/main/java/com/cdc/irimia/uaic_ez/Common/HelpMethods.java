package com.cdc.irimia.uaic_ez.Common;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.cdc.irimia.uaic_ez.StaticObjects.Clazz;
import com.cdc.irimia.uaic_ez.StaticObjects.Faculty;
import com.cdc.irimia.uaic_ez.StaticObjects.Group;
import com.cdc.irimia.uaic_ez.StaticObjects.News;
import com.cdc.irimia.uaic_ez.StaticObjects.Room;
import com.cdc.irimia.uaic_ez.StaticObjects.Teacher;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HelpMethods {


    public static Map<String, Object> getAllObjects() {

        Map<String, Object> map = new HashMap<String, Object>();

        for (Faculty item : Constants.facultiesList) {
            if (!item.getName().equals("faculty...") && item.getName() != null && !item.getName().equals(""))
                map.put(item.getName(), item);
        }

        for (Group item : Constants.groupsList) {
            if (!item.getName().equals("group...") && item.getName() != null && !item.getName().equals(""))
                map.put("Group " + item.getName(), item);
        }

        for (Room item : Constants.roomList) {
            if (item.getName() != null && !item.getName().equals(""))
                map.put("Room " + item.getName() + " " + item.getRef().substring(0, 1).toUpperCase() + item.getRef().substring(1, item.getRef().length()), item);
        }

        for (Teacher item : Constants.teacherList) {
            if (item.getName() != null && !item.getName().equals(""))
                map.put(item.getName(), item);
        }

        for (News item : Constants.newsList) {
            if (item.getTitle() != null && !item.getTitle().equals(""))
                map.put("News: " + item.getTitle(), item);
        }

        for (Clazz item : Constants.classList) {
            if (item.getName() != null && !item.getName().equals(""))
                map.put("Class: " + item.getName(), item);
        }
        return sortByKeys(map);
    }

    public static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap, final boolean order) {

        List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2) {
                if (order) {
                    return o1.getValue().compareTo(o2.getValue());
                } else {
                    return o2.getValue().compareTo(o1.getValue());

                }
            }
        });

        // Maintaining insertion order with the help of LinkedList
        Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }

    public static List<News> newsSorter(List<News> list) {
        Collections.sort(list, new Comparator<News>() {
            @Override
            public int compare(News o1, News o2) {
                if (o1.getImportance() > o2.getImportance()) return -1;
                if (o1.getImportance() < o2.getImportance()) return 1;
                return 0;
            }
        });
        return list;
    }

    public static <K, V> Map<K, V> sortByKeys(Map<K, V> unsortedMap) {
        return new TreeMap<>(unsortedMap);
    }

    public static String URLencode(String url) {
        try {
            String encodeURL = URLEncoder.encode(url, "UTF-8");
            return encodeURL;
        } catch (UnsupportedEncodingException e) {
            return "Issue while encoding" + e.getMessage();
        }
    }

    public static String URLdecode(String url) {
        try {
            String prevURL = "";
            String decodeURL = url;
            while (!prevURL.equals(decodeURL)) {
                prevURL = decodeURL;
                decodeURL = URLDecoder.decode(decodeURL, "UTF-8");
            }
            return decodeURL;
        } catch (UnsupportedEncodingException e) {
            return "Issue while decoding" + e.getMessage();
        }
    }


    public static String md5MyPass(String password) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(password.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String md5(String text){
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte[] hashInBytes = md.digest(text.getBytes(StandardCharsets.UTF_8));

        StringBuilder sb = new StringBuilder();
        for (byte b : hashInBytes) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

    public static boolean emailIsValid(String email) {
        Pattern VALID_EMAIL_ADDRESS_REGEX =
                Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
        return matcher.find();
    }

    public static boolean websiteIsValid(String website) {
        Pattern VALID_EMAIL_ADDRESS_REGEX =
                Pattern.compile("^(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)?[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(\\/.*)?$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(website);
        return matcher.find();
    }

    public static boolean nameIsValid(String name) {
        if (name == null) return false;
        Pattern VALID_NAME_ADDRESS_REGEX =
                Pattern.compile("^[A-Z][a-z]{3,}\\s[A-Z][a-z]{3,}(\\s[A-Z][a-z]{3,})?$", Pattern.UNICODE_CASE);
        Matcher matcher = VALID_NAME_ADDRESS_REGEX.matcher(name);
        return matcher.find();
    }

    public static boolean passIsValid(String password) {
        if (password == null) return false;
        Pattern VALID_PASS_ADDRESS_REGEX =
                Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$", Pattern.UNICODE_CASE);
        Matcher matcher = VALID_PASS_ADDRESS_REGEX.matcher(password);
        return matcher.find();
    }

    public static boolean isInternetAv(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void hideActionBar(Activity activity) {
        ((AppCompatActivity) activity).getSupportActionBar().hide();
    }

    public static void showActionBar(Activity activity) {
        ((AppCompatActivity) activity).getSupportActionBar().show();
    }

    public static int getTotalHeightofListView(ListView listView) {

        ListAdapter mAdapter = listView.getAdapter();

        int totalHeight = 0;

        for (int i = 0; i < mAdapter.getCount(); i++) {
            View mView = mAdapter.getView(i, null, listView);

            mView.measure(
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),

                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

            totalHeight += mView.getMeasuredHeight();
            Log.w("HEIGHT" + i, String.valueOf(totalHeight));

        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        return totalHeight + (listView.getDividerHeight() * (mAdapter.getCount() - 1));

    }

}
