package com.cdc.irimia.uaic_ez.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.cdc.irimia.uaic_ez.Common.HelpMethods;
import com.cdc.irimia.uaic_ez.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SearchFragment extends Fragment {

    ListView listView;
    EditText searchEditText;
    ImageButton imageButton;
    ArrayAdapter<String> itemsAdapter;
    ImageView faceImg;
    TextView lookingTxt;


    public SearchFragment(EditText searchEditText, ImageButton imageButton) {
        this.searchEditText = searchEditText;
        this.imageButton = imageButton;
    }

    public static android.support.v4.app.Fragment newInstance(EditText searchText, ImageButton imageButton) {
        SearchFragment fragment = new SearchFragment(searchText, imageButton);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container != null) {
            container.removeAllViews();
        }
        final View rootView = inflater.inflate(R.layout.fragment_search, container, false);


        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    startDashboardFragment();
                    return true;
                }
                return false;
            }
        });

        listView = rootView.findViewById(R.id.search_listView);
        faceImg = rootView.findViewById(R.id.faceImg);
        lookingTxt = rootView.findViewById(R.id.lookingTxt);

        final Map<String, Object> objectMap = HelpMethods.getAllObjects();
        final List<String> namesList = new ArrayList<>(objectMap.keySet());

        itemsAdapter = new ArrayAdapter<String>(listView.getContext(), android.R.layout.simple_list_item_1, namesList);
        listView.setAdapter(itemsAdapter);
        listView.setTextFilterEnabled(true);
        listView.setVisibility(View.INVISIBLE);
        faceImg.setVisibility(View.VISIBLE);
        lookingTxt.setVisibility(View.VISIBLE);

        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                (SearchFragment.this).itemsAdapter.getFilter().filter(s);
                if (s.toString().equals("") || s.toString() == null) {
                    listView.setVisibility(View.INVISIBLE);
                    faceImg.setVisibility(View.VISIBLE);
                    lookingTxt.setVisibility(View.VISIBLE);
                } else {
                    listView.setVisibility(View.VISIBLE);
                    faceImg.setVisibility(View.INVISIBLE);
                    lookingTxt.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                for (String key : objectMap.keySet()) {
                    if (listView.getItemAtPosition(position).equals(key)) {
                        searchEditText.setText("");
                        startInstanceFragment(objectMap.get(key));
                    }
                }

            }
        });

        return rootView;
    }


    private void startInstanceFragment(Object instance) {
        HelpMethods.hideKeyboard(this.getActivity());
        imageButton.performClick();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, InstanceFragment.newInstance(instance));
        transaction.commit();
    }

    private void startDashboardFragment() {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, DashboardFragment.newInstance());
        transaction.commit();
    }
}
