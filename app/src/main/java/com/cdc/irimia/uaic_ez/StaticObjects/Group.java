package com.cdc.irimia.uaic_ez.StaticObjects;

import java.io.Serializable;

public class Group implements Serializable {

    private String name;
    private Orar orar;
    private String ref;
    private boolean isLoved;
    private int hearts;

    public Group() {
    }

    public Group(String name, String ref) {
        this.name = name;
        this.ref = ref;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Orar getOrar() {
        return orar;
    }

    public void setOrar(Orar orar) {
        this.orar = orar;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public boolean isLoved() {
        return isLoved;
    }

    public void setLoved(boolean loved) {
        isLoved = loved;
    }

    public int getHearts() {
        return hearts;
    }

    public void setHearts(int hearts) {
        this.hearts = hearts;
    }

    @Override
    public String toString() {
        return "Group{" +
                "name='" + name + '\'' +
                ", orar=" + orar +
                ", ref='" + ref + '\'' +
                ", isLoved=" + isLoved +
                ", hearts=" + hearts +
                '}';
    }
}
