package com.cdc.irimia.uaic_ez.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.cdc.irimia.uaic_ez.Common.Constants;
import com.cdc.irimia.uaic_ez.Common.HelpMethods;
import com.cdc.irimia.uaic_ez.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import static com.cdc.irimia.uaic_ez.Common.HelpMethods.emailIsValid;
import static com.cdc.irimia.uaic_ez.Common.HelpMethods.nameIsValid;
import static com.cdc.irimia.uaic_ez.Common.HelpMethods.passIsValid;

public class RegisterActivity extends AppCompatActivity {

    private EditText name;
    private EditText email;
    private EditText password1;
    private EditText password2;
    private Spinner faculty;
    private Spinner year;
    private Spinner group;
    private Button regButton;
    private Context context;
    private FirebaseAuth mAuth;

    private void setNavBarAndStatusBarColors(Activity activity) {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.setNavigationBarColor(getResources().getColor(R.color.uaicBlue));
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(activity, R.color.uaicBlue));

        }
    }

    public void makeToast(View view, String text) {
        int x = view.getLeft() - 200;
        int y = view.getTop() + 2 * view.getHeight() + 80;
        Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP | Gravity.LEFT, x, y);
        toast.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mAuth = FirebaseAuth.getInstance();
        context = this.getApplicationContext();
        askForNet();
        setNavBarAndStatusBarColors(this);
        name = findViewById(R.id.nameEditText);
        email = findViewById(R.id.emailEditText);
        password1 = findViewById(R.id.passwordOne);
        password2 = findViewById(R.id.passwordTwo);
        faculty = findViewById(R.id.facultySpinner);
        year = findViewById(R.id.yearSpinner);
        group = findViewById(R.id.groupSpinner);
        regButton = findViewById(R.id.registerButton);

        name.requestFocus();
        year.setVisibility(View.INVISIBLE);
        group.setVisibility(View.INVISIBLE);


        List<String> facultieNames = new ArrayList<String>();
        List<String> groupNames = new ArrayList<String>();

        facultieNames.add("faculty...");
        groupNames.add("group...");

        facultieNames.addAll(Constants.facultiesStrings.values());
        setAdapter(faculty, facultieNames);


        final ImageView iv1 = findViewById(R.id.infoImg1);
        final ImageView iv2 = findViewById(R.id.infoImg2);
        final ImageView iv3 = findViewById(R.id.infoImg3);
        final ImageView iv4 = findViewById(R.id.infoImg4);
        final ImageView iv5 = findViewById(R.id.infoImg5);
        iv5.setVisibility(View.INVISIBLE);

        setInfoImgClickListenerWithText(iv1, "Example: Popescu Ion");
        setInfoImgClickListenerWithText(iv2, "Example: popescu.ion@email.com");
        setInfoImgClickListenerWithText(iv3, "At least 8 characters (1 Digit, 1 Uppercase, 1 Lowercase)");
        setInfoImgClickListenerWithText(iv4, "The faculty you are studying");
        setInfoImgClickListenerWithText(iv5, "Year and group you are in");


        faculty.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                System.out.println("RA ->" + position);
                if (position != 0) {
                    group.setVisibility(View.VISIBLE);
                    iv5.setVisibility(View.VISIBLE);
                    for (String key : Constants.facultiesStrings.keySet()) {
                        if (Constants.facultiesStrings.get(key).equals(facultieNames.get(position))) {
                            if (Constants.groups.containsKey(key)) {
                                groupNames.addAll((Collection<? extends String>) Constants.groups.get(key));
                                break;
                            }
                        }
                    }
                    setAdapter(group, groupNames);
                } else {

                    group.setVisibility(View.INVISIBLE);
                    iv5.setVisibility(View.INVISIBLE);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String nameText = name.getText().toString();
                String emailText = email.getText().toString();
                String pass1Text = password1.getText().toString();
                String pass2Text = password2.getText().toString();
                String facText = faculty.getSelectedItem().toString();
                String grpText = group.getSelectedItem().toString();
                int checker = 0;
                if (nameIsValid(nameText)) {
                    checker++;
                    setInfoImgClickListenerWithText(iv1, "Example: Popescu Ion");
                } else {
                    checker--;
                    setInfoImgClickListenerWithError(iv1, "Name is not valid! A valid example is Popescu Ion");
                }

                if (emailIsValid(emailText)) {
                    checker++;
                    setInfoImgClickListenerWithText(iv2, "Example: popescu.ion@email.com");
                } else {
                    checker--;
                    setInfoImgClickListenerWithError(iv2, "Email is not valid! A valid example is popescu.ion@email.com");
                }

                if (passIsValid(pass1Text) && pass1Text.equals(pass2Text)) {
                    checker++;
                    setInfoImgClickListenerWithText(iv3, "At least 8 characters (1 Digit, 1 Uppercase, 1 Lowercase)");
                } else {
                    checker--;
                    setInfoImgClickListenerWithError(iv3, "Passwords does not match or they are not valid! Please use at least 8 characters (1 Digit, 1 Uppercase, 1 Lowercase)");
                }

                if (!facText.equals("faculty...")) {
                    checker++;
                    setInfoImgClickListenerWithText(iv4, "The faculty you are studying");
                } else {
                    checker--;
                    setInfoImgClickListenerWithError(iv4, "Please select the faculty you are studying");
                }

                if (grpText.equals("group...")) {
                    checker--;
                    setInfoImgClickListenerWithError(iv5, "Please select the group you are in");
                } else {
                    checker++;
                    setInfoImgClickListenerWithText(iv5, "Group you are in");
                }

                if (checker == 5) {

                    firebaseRegister(emailText, pass1Text, nameText, facText, grpText);
                }
            }
        });
    }

    private void firebaseRegister(String email, String password, String name, String faculty, String group) {
        final String TAG = "registerActivity: ";
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser firebaseUser = mAuth.getCurrentUser();
                            FirebaseFirestore db = FirebaseFirestore.getInstance();
                            String realFaculty = faculty;

                            for (String key : Constants.facultiesStrings.keySet()) {
                                if (Constants.facultiesStrings.get(key).equals(faculty)) {
                                    realFaculty = key;
                                    break;
                                }
                            }

                            HashMap<String, Object> user = new HashMap<>();
                            user.put("userID", firebaseUser.getUid());
                            user.put("name", name);
                            user.put("email", email);
                            user.put("faculty", realFaculty);
                            user.put("group", group);
                            user.put("created_at", new Timestamp(System.currentTimeMillis()));


                            db.collection("users").document(email)
                                    .set(user)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Log.d(TAG, "DocumentSnapshot added with ID: " + email);

                                            }
                                        }
                                    });
                            HashMap<String, Object> newUserNews = new HashMap<>();
                            newUserNews.put("name", "Welcome");
                            newUserNews.put("importance", 11);
                            newUserNews.put("linkOfNews", " ");
                            newUserNews.put("photoLink", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQKxxy0o3GIRoklX4CjP2g8DoQyU4zLCg3tXL7Z9ICiRG5te7zQ3w");
                            newUserNews.put("description", "We are glad you have registered to UAIC Home!");
                            newUserNews.put("readMore", "We are very happy that you have registered, we invite you to visit our website and contact us for any problem.");

                            db.collection("users").document(email).collection("news").add(newUserNews);


                            db.collection("faculties").document(realFaculty).collection("news")
                                    .get()
                                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                            if (task.isSuccessful()) {
                                                for (DocumentSnapshot document : task.getResult()) {
                                                    System.out.println("hai -> a " + document.getId());
                                                    HashMap<String, Object> newNews = new HashMap<>();
                                                    newNews.put("name", document.getData().get("name"));
                                                    newNews.put("importance", document.getData().get("importance"));
                                                    newNews.put("linkOfNews", document.getData().get("linkOfNews"));
                                                    newNews.put("photoLink", document.getData().get("photoLink"));
                                                    newNews.put("description", document.getData().get("description"));
                                                    newNews.put("readMore", document.getData().get("readMore"));

                                                    db.collection("users").document(email).collection("news").add(newNews);

                                                }
                                            } else {
                                                Log.w(TAG, "Error getting documents.", task.getException());
                                            }
                                        }
                                    });


                            // add News to student;
                            /*
                              db.collection("users").document(email).collection("news")
                                    .set(user)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if(task.isSuccessful()){
                                                Log.d(TAG, "DocumentSnapshot added with ID: " + email);

                                            }
                                        }
                                    });
                            db.collection("faculties").document("info").collection("groups")
                                    .get()
                                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                            if (task.isSuccessful()) {
                                                for (DocumentSnapshot document : task.getResult()) {
                                                    Log.d(TAG, document.getId() + " => " + document.getData());
                                                }
                                            } else {
                                                Log.w(TAG, "Error getting documents.", task.getException());
                                            }
                                        }
                                    });


                            db.collection("users").document(email).collection("news")
                                    .set(user)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if(task.isSuccessful()){
                                                Log.d(TAG, "DocumentSnapshot added with ID: " + email);

                                            }
                                        }
                                    });

                                    */

                            Toast.makeText(context, "User registred succesfully", Toast.LENGTH_LONG).show();

                            Runnable runnable = new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Thread.sleep(1000);
                                        startLoginActivity();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }

                                }
                            };
                            Thread thread = new Thread(runnable);
                            thread.start();

                        } else {
                            Toast.makeText(context, "There was an error during registration, please try later", Toast.LENGTH_LONG).show();
                            Runnable runnable = new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Thread.sleep(1000);
                                        startLoginActivity();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            };
                            Thread thread = new Thread(runnable);
                            thread.start();

                        }
                    }
                });
    }


    public void askForNet() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    while (true) {
                        try {
                            if (!HelpMethods.isInternetAv(context)) {
                                RegisterActivity.this.runOnUiThread(new Runnable() {
                                    public void run() {
                                        Toast.makeText(context, "Our app need an internet connection to work properly. Trying to reconnect ...", Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            Thread.sleep(500);
                        } catch (Exception e) {
                        }
                    }
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    private void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        this.finish();
    }


    private void startLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        this.finish();
    }

    private void setInfoImgClickListenerWithError(final ImageView imageView, final String text) {
        imageView.setColorFilter(context.getResources().getColor(R.color.red));
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeToast(v, text);
            }
        });
    }

    private void setInfoImgClickListenerWithText(final ImageView imageView, final String text) {
        imageView.setColorFilter(context.getResources().getColor(R.color.colorPrimaryDark));
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeToast(v, text);
            }
        });
    }

    private void setAdapter(Spinner spinner, List<String> array) {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
                array);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
    }
}
