package com.cdc.irimia.uaic_ez.StaticObjects;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Orar {

    private String link;
    private String type = null;
    private List<Day> dayList;

    public Orar(String link) {
        this.link = link;
        dayList = new ArrayList<>();
    }

    public Orar() {
        dayList = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "Orar{" +
                "link='" + link + '\'' +
                '}';
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void addDay(String name, Object day) {
        Day myDay = new Day(name);

        if (getType() != null) {
            myDay.setType(getType());
        }

        List<Object> aux = new ArrayList<>((Collection<? extends Object>) day);
        for (Object obj : aux) {
            Ora ora = new Ora();
            Map<String, Object> auxMap = new HashMap<>((Map<? extends String, ? extends Object>) obj);

            if (auxMap.containsKey("de_la")) {
                ora.setDe_la(auxMap.get("de_la").toString());
            }
            if (auxMap.containsKey("pana_la")) {
                ora.setPana_la(auxMap.get("pana_la").toString());
            }
            if (auxMap.containsKey("disciplina")) {
                ora.setDisciplina(auxMap.get("disciplina").toString());
            }
            if (auxMap.containsKey("sala")) {
                ora.setSala(auxMap.get("sala").toString());
            }
            if (auxMap.containsKey("tip")) {
                ora.setTip(auxMap.get("tip").toString());
            }
            if (auxMap.containsKey("profesor")) {
                ora.setProfesori(new ArrayList<>((Collection<? extends String>) auxMap.get("profesor")));
            }
            if (auxMap.containsKey("studenti")) {
                ora.setStudenti(new ArrayList<>((Collection<? extends String>) auxMap.get("studenti")));
            }
            myDay.addOra(ora);
        }
        dayList.add(myDay);
    }

    public List<Day> getDayList() {
        return dayList;
    }

    public void setDayList(List<Day> dayList) {
        this.dayList = dayList;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
