package com.cdc.irimia.uaic_ez.Fragments;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cdc.irimia.uaic_ez.Common.Constants;
import com.cdc.irimia.uaic_ez.Common.DownloadImage;
import com.cdc.irimia.uaic_ez.Common.HelpMethods;
import com.cdc.irimia.uaic_ez.R;
import com.cdc.irimia.uaic_ez.StaticObjects.News;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Map;

public class CustomAdapter extends ArrayAdapter<News> implements View.OnClickListener {

    private ArrayList<News> dataSet;
    private DashboardFragment dashboardFragment;
    private Context mContext;
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private int lastPosition = -1;

    public CustomAdapter(DashboardFragment dashboardFragment, ArrayList<News> data, Context context) {
        super(context, R.layout.row_item, data);
        this.dashboardFragment = dashboardFragment;
        this.dataSet = data;
        this.mContext = context;
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();

    }

    @Override
    public void onClick(View v) {

        int position = (Integer) v.getTag();
        Object object = getItem(position);
        News dataModel = (News) object;

        switch (v.getId()) {
            case R.id.item_info: {
                Snackbar.make(v, "You got the first Easter Egg. 14 more to get the prize.", Snackbar.LENGTH_LONG)
                        .setAction("No action", null).show();
                break;
            }

            case R.id.close_img: {
                for (String key : Constants.news.keySet()) {
                    Map<String, Object> map = Constants.news.get(key);
                    try {
                        if (map.get("name").equals(dataModel.getTitle()) && map.get("description").equals(dataModel.getDescription()) && map.get("photoLink").equals(dataModel.getPictureLink()))
                            db.collection("users").document(user.getEmail()).collection("news").document(key).update("deleted", true);
                    } catch (Exception e) {
                    }

                }
                Constants.newsList.remove(dataModel);
                dashboardFragment.refresh();
                break;
            }
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final News dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        final ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_item, parent, false);
            viewHolder.txtName = convertView.findViewById(R.id.name);
            viewHolder.txtType = convertView.findViewById(R.id.type);
            viewHolder.date = convertView.findViewById(R.id.date);
            viewHolder.picture = convertView.findViewById(R.id.item_info);
            viewHolder.close = convertView.findViewById(R.id.close_img);

            result = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        /*
        if(needAnimation) {
            Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
            result.startAnimation(animation);

        }
        */

        lastPosition = position;

        viewHolder.txtName.setText(dataModel.getTitle());
        viewHolder.txtType.setText(dataModel.getDescription());
        viewHolder.date.setText(dataModel.getImportance() + "");

        if (dataModel.getPictureLink() != null && viewHolder.picture.getTag() == null) {
            new Thread() {

                public void run() {
                    new DownloadImage(viewHolder.picture).execute(dataModel.getPictureLink());
                }
            }.start();
        }

        viewHolder.picture.setOnClickListener(this);
        viewHolder.picture.setTag(position);
        viewHolder.close.setOnClickListener(this);
        viewHolder.close.setTag(position);
        return convertView;
    }

    // View lookup cache
    private static class ViewHolder {
        TextView txtName;
        TextView txtType;
        TextView date;
        ImageView picture;
        ImageView close;
    }


}

