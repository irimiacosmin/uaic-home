package com.cdc.irimia.uaic_ez.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.cdc.irimia.uaic_ez.Common.Constants;
import com.cdc.irimia.uaic_ez.Common.HelpMethods;
import com.cdc.irimia.uaic_ez.R;
import com.cdc.irimia.uaic_ez.StaticObjects.Faculty;
import com.cdc.irimia.uaic_ez.StaticObjects.Group;
import com.cdc.irimia.uaic_ez.StaticObjects.Room;
import com.cdc.irimia.uaic_ez.StaticObjects.Teacher;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class EtherFragment extends Fragment {

    private LinearLayout firstLayout;
    private LinearLayout secondLayout;
    private LinearLayout thirdLayout;
    private LinearLayout lastLayout;

    private LinearLayout notList;

    private Object suggestedObject1;
    private Object suggestedObject2;

    private LinearLayout miniTeacher;
    private LinearLayout miniGroups;
    private LinearLayout miniRooms;
    private LinearLayout miniFaculties;

    private LinearLayout maxTeachers;
    private LinearLayout maxGroups;
    private LinearLayout maxRooms;
    private LinearLayout maxFaculties;

    private LinearLayout suggLayout1;
    private TextView suggTextImg1;
    private TextView suggText1;

    private LinearLayout suggLayout2;
    private TextView suggTextImg2;
    private TextView suggText2;

    private View rootView;

    private TextView infoText_ether;
    private ListView etherListView;


    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser user;

    private int grpSize;
    private int tcsSize;
    private int rmsSize;
    private View.OnClickListener facultiesListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setVisibilityBaseLayout(false);
            List<String> favoriteArrayList = new ArrayList<String>();
            List<String> favoriteAuxList = new ArrayList<String>();

            db.collection("users").document(user.getEmail()).collection("loved_faculties").get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                if (!task.getResult().isEmpty()) {
                                    for (DocumentSnapshot documentSnapshot : task.getResult().getDocuments()) {
                                        if (documentSnapshot.getData().containsKey("name") && documentSnapshot.getData().containsKey("faculty")) {
                                            favoriteArrayList.add(documentSnapshot.getData().get("name").toString());
                                            favoriteAuxList.add(documentSnapshot.getData().get("faculty").toString());
                                        }
                                    }
                                }
                            }

                            if (favoriteArrayList.isEmpty()) {
                                etherListView.setVisibility(View.GONE);
                                notList.setVisibility(View.VISIBLE);
                            } else {
                                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, favoriteArrayList);
                                etherListView.setAdapter(arrayAdapter);
                                etherListView.setVisibility(View.VISIBLE);
                                notList.setVisibility(View.GONE);
                                etherListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        String chosen = favoriteArrayList.get(position);
                                        String ref = favoriteAuxList.get(position);
                                        for (Faculty faculty : Constants.facultiesList) {
                                            if (faculty.getName().equals(chosen) && faculty.getRef().equals(ref)) {
                                                startInstanceFragment(faculty);
                                            }
                                        }
                                    }
                                });
                            }

                        }
                    });
        }
    };
    private View.OnClickListener groupsListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setVisibilityBaseLayout(false);
            List<String> favoriteArrayList = new ArrayList<String>();
            List<String> favoriteAuxList = new ArrayList<String>();

            db.collection("users").document(user.getEmail()).collection("loved_groups").get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                if (!task.getResult().isEmpty()) {
                                    for (DocumentSnapshot documentSnapshot : task.getResult().getDocuments()) {
                                        if (documentSnapshot.getData().containsKey("name") && documentSnapshot.getData().containsKey("faculty")) {
                                            favoriteArrayList.add(documentSnapshot.getData().get("name").toString());
                                            favoriteAuxList.add(documentSnapshot.getData().get("faculty").toString());
                                        }
                                    }
                                }
                            }

                            if (favoriteArrayList.isEmpty()) {
                                etherListView.setVisibility(View.GONE);
                                notList.setVisibility(View.VISIBLE);
                            } else {
                                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, favoriteArrayList);
                                etherListView.setAdapter(arrayAdapter);
                                etherListView.setVisibility(View.VISIBLE);
                                notList.setVisibility(View.GONE);
                                etherListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        String chosen = favoriteArrayList.get(position);
                                        String ref = favoriteAuxList.get(position);
                                        for (Group group : Constants.groupsList) {
                                            if (group.getName().equals(chosen) && group.getRef().equals(ref)) {
                                                startInstanceFragment(group);
                                            }
                                        }
                                    }
                                });
                            }

                        }
                    });
        }
    };
    private View.OnClickListener roomsListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setVisibilityBaseLayout(false);
            List<String> favoriteArrayList = new ArrayList<String>();
            List<String> favoriteAuxList = new ArrayList<String>();

            db.collection("users").document(user.getEmail()).collection("loved_rooms").get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                if (!task.getResult().isEmpty()) {
                                    for (DocumentSnapshot documentSnapshot : task.getResult().getDocuments()) {
                                        if (documentSnapshot.getData().containsKey("name") && documentSnapshot.getData().containsKey("faculty")) {
                                            favoriteArrayList.add(documentSnapshot.getData().get("name").toString());
                                            favoriteAuxList.add(documentSnapshot.getData().get("faculty").toString());
                                        }
                                    }
                                }
                            }

                            if (favoriteArrayList.isEmpty()) {
                                etherListView.setVisibility(View.GONE);
                                notList.setVisibility(View.VISIBLE);
                            } else {
                                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, favoriteArrayList);
                                etherListView.setAdapter(arrayAdapter);
                                etherListView.setVisibility(View.VISIBLE);
                                notList.setVisibility(View.GONE);
                                etherListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        String chosen = favoriteArrayList.get(position);
                                        String ref = favoriteAuxList.get(position);

                                        for (Room room : Constants.roomList) {
                                            if (room.getName().equals(chosen) && room.getRef().equals(ref)) {
                                                startInstanceFragment(room);
                                            }
                                        }
                                    }
                                });
                            }

                        }
                    });
        }
    };
    private View.OnClickListener teachersListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            setVisibilityBaseLayout(false);
            List<String> favoriteArrayList = new ArrayList<String>();
            List<String> favoriteAuxList = new ArrayList<String>();

            db.collection("users").document(user.getEmail()).collection("loved_teachers").get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                if (!task.getResult().isEmpty()) {
                                    for (DocumentSnapshot documentSnapshot : task.getResult().getDocuments()) {
                                        if (documentSnapshot.getData().containsKey("name") && documentSnapshot.getData().containsKey("faculty")) {
                                            favoriteArrayList.add(documentSnapshot.getData().get("name").toString());
                                            favoriteAuxList.add(documentSnapshot.getData().get("faculty").toString());
                                        }
                                    }
                                }
                            }

                            if (favoriteArrayList.isEmpty()) {
                                etherListView.setVisibility(View.GONE);
                                notList.setVisibility(View.VISIBLE);
                            } else {
                                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, favoriteArrayList);
                                etherListView.setAdapter(arrayAdapter);
                                etherListView.setVisibility(View.VISIBLE);
                                notList.setVisibility(View.GONE);
                                etherListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        String chosen = favoriteArrayList.get(position);
                                        String ref = favoriteAuxList.get(position);
                                        for (Teacher teacher : Constants.teacherList) {
                                            if (teacher.getName().equals(chosen) && teacher.getRef().equals(ref)) {
                                                startInstanceFragment(teacher);
                                            }
                                        }
                                    }
                                });
                            }

                        }
                    });
        }
    };

    public static Fragment newInstance() {
        EtherFragment fragment = new EtherFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container != null) {
            container.removeAllViews();
        }
        View rootview = inflater.inflate(R.layout.fragment_ether, container, false);
        this.rootView = rootview;
        HelpMethods.showActionBar(this.getActivity());

        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();


        firstLayout = rootview.findViewById(R.id.firstLayout);
        secondLayout = rootview.findViewById(R.id.secondLayout);
        thirdLayout = rootview.findViewById(R.id.thirdLayout);
        lastLayout = rootview.findViewById(R.id.lastLayout);
        notList = rootview.findViewById(R.id.notList);

        maxTeachers = rootview.findViewById(R.id.maxTeachers);
        maxGroups = rootview.findViewById(R.id.maxGroups);
        maxRooms = rootview.findViewById(R.id.maxRooms);
        maxFaculties = rootview.findViewById(R.id.maxFaculties);

        miniTeacher = rootview.findViewById(R.id.miniTeachers);
        miniGroups = rootview.findViewById(R.id.miniGroups);
        miniRooms = rootview.findViewById(R.id.miniRooms);
        miniFaculties = rootview.findViewById(R.id.miniFaculties);

        suggLayout1 = rootview.findViewById(R.id.ether_sugg_layout1);
        suggTextImg1 = rootview.findViewById(R.id.ether_sugg_photo1);
        suggText1 = rootview.findViewById(R.id.ether_sugg_text1);

        suggLayout2 = rootview.findViewById(R.id.ether_sugg_layout2);
        suggTextImg2 = rootview.findViewById(R.id.ether_sugg_photo2);
        suggText2 = rootview.findViewById(R.id.ether_sugg_text2);


        infoText_ether = rootview.findViewById(R.id.infoText_ether);
        etherListView = rootview.findViewById(R.id.etherListView);


        lastLayout.setVisibility(View.GONE);
        notList.setVisibility(View.GONE);
        callListeners();
        addSuggestions();
        return rootview;
    }

    private void addSuggestions() {

        Map<String, Integer> bigMap = new HashMap<>();
        bigMap.putAll(Constants.groupsPriority);
        bigMap.putAll(Constants.teachersPriority);
        bigMap.putAll(Constants.roomsPriority);

        grpSize = Constants.groupsPriority.size();
        tcsSize = Constants.teachersPriority.size();
        rmsSize = Constants.roomsPriority.size();

        bigMap = HelpMethods.sortByComparator(bigMap, false);
        if (bigMap.size() > 1) {

            Iterator<Map.Entry<String, Integer>> iterator = bigMap.entrySet().iterator();

            String key1 = iterator.next().getKey();
            String key2 = iterator.next().getKey();


            if (Constants.groupsPriority.containsKey(key1)) {
                suggText1.setText("Group " + key1);
                suggTextImg1.setText(key1.substring(key1.length() - 2, key1.length()).toUpperCase());
                for (Group group : Constants.groupsList) {
                    if (group.getName().equals(key1)) {
                        suggestedObject1 = group;
                    }
                }
            } else if (Constants.roomsPriority.containsKey(key1)) {
                suggTextImg1.setText("RO");
                suggText1.setText("Room " + key1);
                for (Room room : Constants.roomList) {
                    if (room.getName().equals(key1)) {
                        suggestedObject1 = room;
                    }
                }
            } else {
                String[] parts = key1.split(" ");
                suggText1.setText(key1);
                suggTextImg1.setText((parts[parts.length - 2].substring(0, 1) + parts[parts.length - 1].substring(0, 1)).toUpperCase());
                for (Teacher teacher : Constants.teacherList) {
                    if (teacher.getName().equals(key1)) {
                        suggestedObject1 = teacher;
                    }
                }
            }

            if (Constants.groupsPriority.containsKey(key2)) {
                suggText2.setText("Group " + key2);
                suggTextImg2.setText(key2.substring(key2.length() - 2, key2.length()).toUpperCase());
                for (Group group : Constants.groupsList) {
                    if (group.getName().equals(key2)) {
                        suggestedObject2 = group;
                    }
                }
            } else if (Constants.roomsPriority.containsKey(key2)) {
                suggTextImg2.setText("RO");
                suggText2.setText("Room " + key2);
                for (Room room : Constants.roomList) {
                    if (room.getName().equals(key2)) {
                        suggestedObject2 = room;
                    }
                }
            } else {
                String[] parts = key2.split(" ");
                suggText2.setText(key2);
                suggTextImg2.setText((parts[parts.length - 2].substring(0, 1) + parts[parts.length - 1].substring(0, 1)).toUpperCase());
                for (Teacher teacher : Constants.teacherList) {
                    if (teacher.getName().equals(key2)) {
                        suggestedObject2 = teacher;
                    }
                }
            }

            suggLayout1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startInstanceFragment(suggestedObject1);
                }
            });

            suggLayout2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startInstanceFragment(suggestedObject2);
                }
            });

        } else {
            thirdLayout.setVisibility(View.INVISIBLE);
            infoText_ether.setVisibility(View.INVISIBLE);
        }


    }

    private void setVisibilityBaseLayout(boolean flag) {
        if (flag) {
            AlphaAnimation animation1 = new AlphaAnimation(0.0f, 1.0f);
            animation1.setDuration(1000);
            animation1.setFillAfter(true);
            rootView.startAnimation(animation1);
            firstLayout.setVisibility(View.VISIBLE);
            secondLayout.setVisibility(View.VISIBLE);
            thirdLayout.setVisibility(View.VISIBLE);
            infoText_ether.setVisibility(View.VISIBLE);
            lastLayout.setVisibility(View.GONE);
        } else {
            AlphaAnimation animation1 = new AlphaAnimation(0.0f, 1.0f);
            animation1.setDuration(2000);
            animation1.setFillAfter(true);
            etherListView.startAnimation(animation1);
            firstLayout.setVisibility(View.GONE);
            secondLayout.setVisibility(View.GONE);
            thirdLayout.setVisibility(View.GONE);
            infoText_ether.setVisibility(View.GONE);
            lastLayout.setVisibility(View.VISIBLE);
        }
    }

    private void callListeners() {

        miniGroups.setOnClickListener(groupsListener);
        maxGroups.setOnClickListener(groupsListener);

        miniRooms.setOnClickListener(roomsListener);
        maxRooms.setOnClickListener(roomsListener);

        miniFaculties.setOnClickListener(facultiesListener);
        maxFaculties.setOnClickListener(facultiesListener);

        miniTeacher.setOnClickListener(teachersListener);
        maxTeachers.setOnClickListener(teachersListener);


    }

    private void startInstanceFragment(Object instance) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, InstanceFragment.newInstance(instance));
        transaction.commit();
    }


}

