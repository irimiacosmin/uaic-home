package com.cdc.irimia.uaic_ez.StaticObjects;

public class Building extends BuildingContainer {

    private String description;
    private String phoneNo;
    private Orar orar;


    public Building(String name, String shortName, Double xCo, Double yCo, String description, String phoneNo, Orar orar) {
        super(name, shortName, xCo, yCo);
        this.description = description;
        this.phoneNo = phoneNo;
        this.orar = orar;
    }


    @Override
    public String toString() {
        return "Building{" +
                "description='" + description + '\'' +
                ", phoneNo='" + phoneNo + '\'' +
                ", orar=" + orar +
                '}';
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public Orar getOrar() {
        return orar;
    }

    public void setOrar(Orar orar) {
        this.orar = orar;
    }
}
