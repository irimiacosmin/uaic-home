package com.cdc.irimia.uaic_ez.Objects;

public class Response {

    private String error = null;
    private String mentions = null;
    private User user = null;

    public Response(String error, String mentions, User user) {
        this.error = error;
        this.mentions = mentions;
        this.user = user;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMentions() {
        return mentions;
    }

    public void setMentions(String mentions) {
        this.mentions = mentions;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Response{" +
                "error='" + error + '\'' +
                ", mentions='" + mentions + '\'' +
                ", user=" + user +
                '}';
    }
}
