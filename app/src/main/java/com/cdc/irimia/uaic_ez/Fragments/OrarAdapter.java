package com.cdc.irimia.uaic_ez.Fragments;


import android.content.Context;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cdc.irimia.uaic_ez.Common.HelpMethods;
import com.cdc.irimia.uaic_ez.R;
import com.cdc.irimia.uaic_ez.StaticObjects.Day;
import com.cdc.irimia.uaic_ez.StaticObjects.Ora;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

public class OrarAdapter extends ArrayAdapter<Day> implements View.OnClickListener {
    private InstanceFragment instanceFragment;
    private Context mContext;
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser user;

    public OrarAdapter(InstanceFragment instanceFragment, List<Day> orar, Context context) {

        super(context, R.layout.day_item, orar);
        this.instanceFragment = instanceFragment;
        this.mContext = context;
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();

    }

    @Override
    public void onClick(View v) {
        /*
        int position=(Integer) v.getTag();
        Object object = getItem(position);
        News dataModel = (News)object;

        switch (v.getId())
        {
            case R.id.item_info: {
                Snackbar.make(v, "You got the second Easter Egg. 13 more to get the prize.", Snackbar.LENGTH_LONG)
                        .setAction("No action", null).show();
                break;
            }
        }
        */
    }

    private String trimMyString(String str) {
        if (str == null)
            return "";
        if (str.length() < 52) {
            return str;
        }
        return str.substring(0, 52);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Day dataModel = getItem(position);
        final ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.day_item, parent, false);
            viewHolder.dayName = convertView.findViewById(R.id.dayName);
            viewHolder.ore = convertView.findViewById(R.id.ore);
            viewHolder.dayLayout = convertView.findViewById(R.id.dayLayout);

            result = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        String tab = "&#8195;";
        viewHolder.dayName.setText(dataModel.getName());
        List<Spanned> oreList = new ArrayList<>();
        for (Ora ora : dataModel.getOraList()) {
            String type = dataModel.getType();
            String sourceCode = "<b>" + ora.getDe_la() + " - " + ora.getPana_la() + "</b>";
            String spart2 = "<small>" + " (" + ora.getTip() + ")</small><br/>" + "<small>" + trimMyString(ora.getDisciplina()) + "</small>";
            String profs = "";
            String studs = "";
            String sala = "";

            if (type != null) {
                switch (type) {
                    case "class": {
                        sourceCode = sourceCode + spart2;
                        profs = "<br/><small>";
                        int i = 0;
                        for (String pr : ora.getProfesori()) {
                            if (i == 2) break;
                            i++;
                            profs = profs + pr + tab;
                        }
                        profs = profs + "</small>";
                        studs = "<br/><small>";
                        for (String pr : ora.getStudenti()) {
                            studs = studs + pr + tab;
                        }
                        studs = studs + "</small>";
                        sala = "<br/><small>Room:  " + ora.getSala() + "</small>";
                        break;
                    }
                    case "group": {
                        sourceCode = sourceCode + spart2;
                        profs = "<br/><small>";
                        int i = 0;
                        for (String pr : ora.getProfesori()) {
                            if (i == 2) break;
                            i++;
                            profs = profs + pr + tab;
                        }
                        profs = profs + "</small>";
                        sala = "<br/><small>Room: " + ora.getSala() + "</small>";
                        break;
                    }
                    case "room": {
                        sourceCode = sourceCode + spart2;
                        profs = "<br/><small>";
                        int i = 0;
                        for (String pr : ora.getProfesori()) {
                            if (i == 2) break;
                            i++;
                            profs = profs + pr + tab;
                        }
                        profs = profs + "</small>";
                        studs = "<br/><small>";
                        for (String pr : ora.getStudenti()) {
                            studs = studs + pr + tab;
                        }
                        studs = studs + "</small>";
                        break;
                    }
                    case "prof": {
                        sourceCode = sourceCode + spart2;
                        studs = "<br/><small>";
                        for (String pr : ora.getStudenti()) {
                            studs = studs + pr + tab;
                        }
                        studs = studs + "</small>";
                        sala = "<br/><small>Room: " + ora.getSala() + "</small>";
                        break;
                    }
                }
                // + "<small>" + ora.getDisciplina() + "</small>" + "<br />" + "<small>" + ora.getDisciplina() + "</small>"
            }
            Spanned text = Html.fromHtml(sourceCode + profs + studs + sala + "<br/>");
            oreList.add(text);
        }

        ArrayAdapter<Spanned> itemsAdapter = new ArrayAdapter<Spanned>(parent.getContext(), android.R.layout.simple_list_item_1, oreList);
        viewHolder.ore.setAdapter(itemsAdapter);
        ViewGroup.LayoutParams layoutParams = viewHolder.dayLayout.getLayoutParams();
        layoutParams.height = 150 + HelpMethods.getTotalHeightofListView(viewHolder.ore);

       /* viewHolder.txtType.setText(dataModel.getDescription());
        viewHolder.date.setText(dataModel.getImportance()+"");


        viewHolder.picture.setOnClickListener(this);
        viewHolder.picture.setTag(position);
        viewHolder.close.setOnClickListener(this);

        viewHolder.close.setTag(position);
        */
        return convertView;
    }

    // View lookup cache
    private static class ViewHolder {
        RelativeLayout dayLayout;
        TextView dayName;
        ListView ore;
    }


}

