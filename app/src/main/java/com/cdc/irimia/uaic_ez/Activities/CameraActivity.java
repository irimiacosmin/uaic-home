package com.cdc.irimia.uaic_ez.Activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cdc.irimia.uaic_ez.Common.Constants;
import com.cdc.irimia.uaic_ez.Common.HelpMethods;
import com.cdc.irimia.uaic_ez.Common.StringSimilarity;
import com.cdc.irimia.uaic_ez.R;
import com.cdc.irimia.uaic_ez.StaticObjects.Faculty;
import com.cdc.irimia.uaic_ez.StaticObjects.Group;
import com.cdc.irimia.uaic_ez.StaticObjects.LevObjectHolder;
import com.cdc.irimia.uaic_ez.StaticObjects.Room;
import com.cdc.irimia.uaic_ez.StaticObjects.Teacher;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionCloudTextRecognizerOptions;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class CameraActivity extends AppCompatActivity {


    private Camera mCamera;
    private CameraPreview mPreview;
    private Context context;
    private FrameLayout preview;
    private ImageView camera_img;
    private LinearLayout camera_analyzing;
    private LinearLayout camera_make_instance;
    private LinearLayout camera_bottom_layout;
    private TextView camera_txt_progress;
    private ListView camera_list;
    private TextView camera_message_to_show;

    private Map<String, LevObjectHolder> allThings = new HashMap<String, LevObjectHolder>();
    private byte[] picture = null;
    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            setPicture(data);
            camera_txt_progress.setText("Analyzing your photo...");
            Bitmap picture = BitmapFactory.decodeByteArray(data, 0, data.length);

            FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(picture);
            FirebaseVisionCloudTextRecognizerOptions options =
                    new FirebaseVisionCloudTextRecognizerOptions.Builder()
                            .setLanguageHints(Arrays.asList("en", "ro"))
                            .build();
            FirebaseVisionTextRecognizer textRecognizer = FirebaseVision.getInstance()
                    .getCloudTextRecognizer(options);

            camera_img.setImageBitmap(rotateBitmap(picture, 90));
            preview.setVisibility(View.GONE);
            camera_img.setVisibility(View.VISIBLE);

            textRecognizer.processImage(image)
                    .addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
                        @Override
                        public void onSuccess(FirebaseVisionText result) {
                            camera_txt_progress.setText("Searching in our database for the optimal result");
                            analyzeStringAlgorithm(result.getText());
                        }
                    })
                    .addOnFailureListener(
                            new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    camera_txt_progress.setText("Hmm, I don't think I've got this right ... Sorry!");
                                }
                            });

        }
    };

    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e) {
        }
        return c;
    }

    private void setNavBarAndStatusBarColors(Activity activity) {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.setNavigationBarColor(getResources().getColor(R.color.uaicBlue));
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(activity, R.color.uaicBlue));
        }
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] data) {
        picture = data;
    }

    private void analyzeStringAlgorithm(String fromPhoto) {
        String[] parts = fromPhoto.replace("\n", " ").split(" ");
        List<String> stringList = new ArrayList<>(Arrays.asList(parts));
        List<String> aux = new ArrayList<>();
        for (int i = 0; i < stringList.size(); i++) {
            for (int j = 0; j < stringList.size(); j++) {
                if (!stringList.get(i).equals(stringList.get(j))) {
                    aux.add(stringList.get(i) + stringList.get(j));
                }
            }
        }
        stringList.addAll(aux);

        for (String key : allThings.keySet()) {
            String newStr = key;

            if (key.startsWith("Facultatea")) {
                newStr = key.substring(14, key.length());
            } else if (key.startsWith("Lect")) {
                newStr = key.substring(10, key.length());
            } else if (key.startsWith("Asist")) {
                newStr = key.substring(11, key.length());
            } else if (key.startsWith("Drd")) {
                newStr = key.substring(5, key.length());
            } else if (key.startsWith("Conf")) {
                newStr = key.substring(10, key.length());
            } else if (key.startsWith("Prof")) {
                newStr = key.substring(10, key.length());
            }
            for (String str : stringList) {
                allThings.get(key).addSimilarityToMap(str, StringSimilarity.compare(newStr, str));
            }
        }
        camera_txt_progress.setText("Done");


        Map<LevObjectHolder, Double> levMap = new HashMap<>();
        for (String key : allThings.keySet()) {
            Double percent = allThings.get(key).getBestPercentage();
            if (percent >= 0.80) {
                levMap.put(allThings.get(key), percent);
            }
        }

        if (levMap.isEmpty()) {
            showNotFound();
        } else {
            showInstance(levMap);
        }

    }

    private void showInstance(Map<LevObjectHolder, Double> levMap) {
        camera_analyzing.setVisibility(View.GONE);
        camera_make_instance.setVisibility(View.VISIBLE);
        camera_message_to_show.setText("Hmm, i think i see ...");

        List<String> stringArrayList = new ArrayList<String>();

        for (LevObjectHolder obj : levMap.keySet()) {
            stringArrayList.add(obj.getName());
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1, stringArrayList) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView tv = view.findViewById(android.R.id.text1);

                tv.setTextColor(getResources().getColor(R.color.white));
                return view;
            }
        };

        camera_list.setAdapter(arrayAdapter);

        camera_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String compare = arrayAdapter.getItem(position);
                for (LevObjectHolder item : levMap.keySet()) {
                    if (item.getName().equals(compare)) {
                        startMainActivity(item.getWhatItIs(), item.getFacultyRef(), item.getName());
                    }
                }
            }
        });
        camera_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMainActivity(0, null, null);
            }
        });


    }

    private void showNotFound() {
        camera_txt_progress.setText("I'm sorry, I couldn't find anything ...");
        camera_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMainActivity(0, null, null);
            }
        });
    }

    private Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    private void previewListener() {

        mPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCamera.takePicture(null, null, mPicture);
                mPreview.setOnClickListener(null);
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        context = this;
        askForNet();
        setNavBarAndStatusBarColors(this);
        CameraActivityPermissionsDispatcher.needStoragePerWithPermissionCheck(this);
        CameraActivityPermissionsDispatcher.onPermissionRequestWithPermissionCheck(this);
        setContentView(R.layout.activity_camera);
        final Context context = this.getApplicationContext();

        camera_analyzing = findViewById(R.id.camera_analyzing);
        camera_make_instance = findViewById(R.id.camera_make_instance);
        camera_bottom_layout = findViewById(R.id.camera_bottom_layout);
        camera_txt_progress = findViewById(R.id.camera_txt_progress);
        camera_message_to_show = findViewById(R.id.camera_message_to_show);
        camera_list = findViewById(R.id.camera_list);
        camera_img = findViewById(R.id.camera_img);

        mCamera = getCameraInstance();
        Camera.Parameters params = mCamera.getParameters();
        params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        params.set("orientation", "portrait");
        params.set("rotation", 90);
        params.set("jpeg-quality", 70);
        params.setPictureFormat(PixelFormat.RGB_565);
        params.setPictureSize(800, 600);
        mCamera.setParameters(params);

        mPreview = new CameraPreview(this, mCamera);
        preview = findViewById(R.id.camera_preview);
        preview.addView(mPreview);

        addToAllThings();
        previewListener();

    }

    private boolean itemIsValid(String name, String ref) {
        return name != null && !name.equals("") && ref != null && !ref.equals("");
    }

    // 1 = faculty | 2 teacher | 3 room | 4 group
    // String name, int whatItIs, String facultyRef, String groupRef, String roomRef, String reacherRef
    public void addToAllThings() {
        for (Faculty item : Constants.facultiesList) {
            if (!item.getName().equals("faculty...") && itemIsValid(item.getName(), item.getRef()))
                allThings.put(item.getName(), new LevObjectHolder(item.getName(), 1, item.getRef()));
        }

        for (Group item : Constants.groupsList) {
            if (!item.getName().equals("group...") && itemIsValid(item.getName(), item.getRef()))
                allThings.put(item.getName(), new LevObjectHolder(item.getName(), 4, item.getRef()));
        }

        for (Room item : Constants.roomList) {
            if (itemIsValid(item.getName(), item.getRef()))
                allThings.put(item.getName(), new LevObjectHolder(item.getName(), 3, item.getRef()));
        }

        for (Teacher item : Constants.teacherList) {
            if (itemIsValid(item.getName(), item.getRef()))
                allThings.put(item.getName(), new LevObjectHolder(item.getName(), 2, item.getRef()));
        }
    }

    private void startMainActivity(int whatIsIt, String ref, String name) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("whatIsIt", whatIsIt);
        intent.putExtra("facultyRef", ref);
        intent.putExtra("itemName", name);

        startActivity(intent);
        this.finish();
    }


    public void askForNet() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    while (true) {
                        try {
                            if (!HelpMethods.isInternetAv(context)) {
                                CameraActivity.this.runOnUiThread(new Runnable() {
                                    public void run() {
                                        Toast.makeText(context, "Our app need an internet connection to work properly. Trying to reconnect ...", Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            Thread.sleep(500);
                        } catch (Exception e) {
                        }
                    }
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }


    @NeedsPermission({Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    void onPermissionRequest() {
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        CameraActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
        this.recreate();
    }

    @OnPermissionDenied({Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    void onPermissionDeny() {
    }


    @NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    void needStoragePer() {
    }

    @OnPermissionDenied({Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    void denyStoragePer() {
    }
}
