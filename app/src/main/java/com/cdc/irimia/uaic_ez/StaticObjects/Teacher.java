package com.cdc.irimia.uaic_ez.StaticObjects;

import java.io.Serializable;

public class Teacher implements Serializable {

    private String name;
    private String phoneNo;
    private String email;
    private Orar orar;
    private Room room;
    private String ref;
    private String website;
    private boolean isLoved;
    private int hearts;

    public Teacher() {
    }

    public Teacher(String name, String ref) {
        this.name = name;
        this.ref = ref;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Orar getOrar() {
        return orar;
    }

    public void setOrar(Orar orar) {
        this.orar = orar;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public boolean isLoved() {
        return isLoved;
    }

    public void setLoved(boolean loved) {
        isLoved = loved;
    }

    public int getHearts() {
        return hearts;
    }

    public void setHearts(int hearts) {
        this.hearts = hearts;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "name='" + name + '\'' +
                ", phoneNo='" + phoneNo + '\'' +
                ", email='" + email + '\'' +
                ", orar=" + orar +
                ", room=" + room +
                ", ref='" + ref + '\'' +
                ", isLoved=" + isLoved +
                ", hearts=" + hearts +
                '}';
    }
}
