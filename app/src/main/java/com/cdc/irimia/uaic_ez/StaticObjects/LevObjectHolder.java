package com.cdc.irimia.uaic_ez.StaticObjects;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class LevObjectHolder {
    private String name;
    private Map<String, Double> stringSimilarityMap;

    private int whatItIs;
    // 1 = faculty | 2 teacher | 3 room | 4 group

    private String facultyRef;

    public LevObjectHolder(String name, int whatItIs, String facultyRef) {
        this.name = name;
        this.whatItIs = whatItIs;
        this.facultyRef = facultyRef;
        stringSimilarityMap = new HashMap<>();
    }

    public void addSimilarityToMap(String str, Double procent) {
        stringSimilarityMap.put(str, procent);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Double> getStringSimilarityMap() {
        return stringSimilarityMap;
    }

    public void setStringSimilarityMap(Map<String, Double> stringSimilarityMap) {
        this.stringSimilarityMap = stringSimilarityMap;
    }

    public int getWhatItIs() {
        return whatItIs;
    }

    public void setWhatItIs(int whatItIs) {
        this.whatItIs = whatItIs;
    }

    public String getFacultyRef() {
        return facultyRef;
    }

    public void setFacultyRef(String facultyRef) {
        this.facultyRef = facultyRef;
    }

    public Double getBestPercentage() {
        return Collections.max(stringSimilarityMap.values());
    }

    @Override
    public String toString() {
        return "LevObjectHolder{" +
                ", stringSimilarityMap=" + stringSimilarityMap +
                '}';
    }

}
