package com.cdc.irimia.uaic_ez.Fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.text.Spanned;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cdc.irimia.uaic_ez.Activities.MainActivity;
import com.cdc.irimia.uaic_ez.Common.Constants;
import com.cdc.irimia.uaic_ez.Common.HelpMethods;
import com.cdc.irimia.uaic_ez.R;
import com.cdc.irimia.uaic_ez.StaticObjects.Building;
import com.cdc.irimia.uaic_ez.StaticObjects.Clazz;
import com.cdc.irimia.uaic_ez.StaticObjects.Day;
import com.cdc.irimia.uaic_ez.StaticObjects.Faculty;
import com.cdc.irimia.uaic_ez.StaticObjects.Group;
import com.cdc.irimia.uaic_ez.StaticObjects.News;
import com.cdc.irimia.uaic_ez.StaticObjects.Ora;
import com.cdc.irimia.uaic_ez.StaticObjects.Orar;
import com.cdc.irimia.uaic_ez.StaticObjects.Room;
import com.cdc.irimia.uaic_ez.StaticObjects.Teacher;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class InstanceFragment extends Fragment {


    private static OrarAdapter adapterOrar;
    public static boolean canSendData = true;
    boolean groupsIsClicked = false, teachersIsClicked = false, roomsIsClicked = false, scheduleIsClicked = false;
    RelativeLayout fragment_instance_layout;
    ImageView imageCover;
    ImageView imageMiddleRound;
    ImageView imageFavorite;
    ImageView helpInstance;
    ImageView searchInstance;
    TextView textTitle;
    TextView textDescription;
    TextView moreString;
    LinearLayout layoutLocation;
    TextView textLocation;
    LinearLayout layoutPhone;
    TextView textPhone;
    LinearLayout layoutEmail;
    TextView textEmail;
    LinearLayout layoutWebsite;
    TextView textWebsite;
    LinearLayout layoutSchedule;
    ImageView scheduleImg;
    TextView multifunctionText;
    LinearLayout layoutLaunchMaps;
    LinearLayout layoutGroups;
    ListView listGroups;
    TextView groupsNames;
    LinearLayout layoutRooms;
    ListView listRooms;
    TextView roomsNames;
    LinearLayout layoutTeachers;
    TextView teachersNames;
    ListView listTeachers;
    private Object instance;
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private InstanceFragment fragment;
    private Context context;
    private Faculty faculty;
    private boolean loveIsPressed;
    private AlertDialog alertDialog;
    private ListView orar;
    private View.OnClickListener phoneClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (hasCallPermission()) {
                String[] parts = textPhone.getText().toString().split(";");
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + parts[0]));
                startActivity(intent);
            } else {
                InstanceFragmentPermissionsDispatcher.needPermissionWithPermissionCheck(fragment);
            }
        }
    };
    private View.OnClickListener emailClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String[] TO = {textEmail.getText().toString()};
            Intent emailIntent = new Intent(Intent.ACTION_SEND);

            emailIntent.setData(Uri.parse("mailto:"));
            emailIntent.setType("text/plain");
            emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your subject");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here");
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        }
    };
    private View.OnClickListener webClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(HelpMethods.URLdecode(textWebsite.getText().toString())));
            startActivity(i);
        }
    };
    private View.OnClickListener mapsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(Intent.ACTION_VIEW);

            double latitude = faculty.getGeoPoint().getLatitude();
            double longitude = faculty.getGeoPoint().getLongitude();
            String uriBegin = "geo:" + latitude + "," + longitude;
            String query = latitude + "," + longitude + "(" + faculty.getName() + ")";
            String encodedQuery = Uri.encode(query);
            String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";

            i.setData(Uri.parse(uriString));
            i.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
            startActivity(i);

        }
    };
    private View.OnClickListener roomsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (roomsIsClicked) {
                listRooms.setVisibility(View.GONE);
                roomsNames.setVisibility(View.VISIBLE);

                ViewGroup.LayoutParams layoutParams = layoutRooms.getLayoutParams();
                layoutParams.height = 250;
                layoutRooms.setLayoutParams(layoutParams);
                roomsIsClicked = false;
            } else {
                listRooms.setVisibility(View.VISIBLE);
                roomsNames.setVisibility(View.GONE);

                ViewGroup.LayoutParams layoutParams = layoutRooms.getLayoutParams();
                layoutParams.height = 100 + HelpMethods.getTotalHeightofListView(listRooms);
                layoutRooms.setLayoutParams(layoutParams);
                roomsIsClicked = true;
            }
        }
    };
    private View.OnClickListener teachersClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (teachersIsClicked) {
                listTeachers.setVisibility(View.GONE);
                teachersNames.setVisibility(View.VISIBLE);
                ViewGroup.LayoutParams layoutParams = layoutTeachers.getLayoutParams();
                layoutParams.height = 250;
                layoutTeachers.setLayoutParams(layoutParams);
                teachersIsClicked = false;
            } else {
                listTeachers.setVisibility(View.VISIBLE);
                teachersNames.setVisibility(View.GONE);
                ViewGroup.LayoutParams layoutParams = layoutTeachers.getLayoutParams();
                layoutParams.height = 100 + HelpMethods.getTotalHeightofListView(listTeachers);
                layoutTeachers.setLayoutParams(layoutParams);
                teachersIsClicked = true;
            }
        }
    };
    private View.OnClickListener groupsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (groupsIsClicked) {
                listGroups.setVisibility(View.GONE);
                groupsNames.setVisibility(View.VISIBLE);
                ViewGroup.LayoutParams layoutParams = layoutGroups.getLayoutParams();
                layoutParams.height = 250;
                layoutGroups.setLayoutParams(layoutParams);
                groupsIsClicked = false;
            } else {
                listGroups.setVisibility(View.VISIBLE);
                groupsNames.setVisibility(View.GONE);

                ViewGroup.LayoutParams layoutParams = layoutGroups.getLayoutParams();
                layoutParams.height = 100 + HelpMethods.getTotalHeightofListView(listGroups);
                layoutGroups.setLayoutParams(layoutParams);
                groupsIsClicked = true;
            }
        }
    };
    private AdapterView.OnItemClickListener roomsItemListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            startInstanceFragment(((Faculty) instance).getRooms().get(position));
        }
    };
    private AdapterView.OnItemClickListener groupsItemListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            startInstanceFragment(((Faculty) instance).getGroups().get(position));
        }
    };
    private AdapterView.OnItemClickListener teachersItemListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            startInstanceFragment(((Faculty) instance).getTeachers().get(position));
        }
    };

    public InstanceFragment(Object instance) {
        this.instance = instance;
        this.fragment = this;
    }

    public static Fragment newInstance(Object instance) {
        InstanceFragment fragment = new InstanceFragment(instance);
        return fragment;
    }

    public void setRandomImage(String objType) {
        Random rand = new Random();
        int result = rand.nextInt(9 - 1) + 1;

        switch (result) {
            case 1: {
                imageCover.setImageResource(R.drawable.wallp1);
                break;
            }
            case 2: {
                imageCover.setImageResource(R.drawable.wallp2);
                break;
            }
            case 3: {
                imageCover.setImageResource(R.drawable.wallp3);
                break;
            }
            case 4: {
                imageCover.setImageResource(R.drawable.wallp4);
                break;
            }
            case 5: {
                imageCover.setImageResource(R.drawable.wallp5);
                break;
            }
            case 6: {
                imageCover.setImageResource(R.drawable.wallp6);
                break;
            }
            case 7: {
                imageCover.setImageResource(R.drawable.wallp7);
                break;
            }
            case 8: {
                imageCover.setImageResource(R.drawable.wallp8);
                break;
            }
            case 9: {
                imageCover.setImageResource(R.drawable.wallp9);
                break;
            }
        }

    }

    public void findThemAll(View rootView) {

        fragment_instance_layout = rootView.findViewById(R.id.fragment_instance_layout);
        imageCover = rootView.findViewById(R.id.header_cover_image);
        searchInstance = rootView.findViewById(R.id.search_instance);
        imageMiddleRound = rootView.findViewById(R.id.user_profile_photo);
        imageFavorite = rootView.findViewById(R.id.imageFavorite);
        textTitle = rootView.findViewById(R.id.user_profile_name);
        textDescription = rootView.findViewById(R.id.user_profile_short_bio);

        orar = rootView.findViewById(R.id.instaceOrar);

        layoutLocation = rootView.findViewById(R.id.layoutLocation);
        textLocation = rootView.findViewById(R.id.textLocation);

        layoutPhone = rootView.findViewById(R.id.layoutPhone);
        textPhone = rootView.findViewById(R.id.textPhone);

        layoutEmail = rootView.findViewById(R.id.layoutEmail);
        textEmail = rootView.findViewById(R.id.textEmail);

        layoutWebsite = rootView.findViewById(R.id.layoutWebsite);
        textWebsite = rootView.findViewById(R.id.textWebsite);

        layoutSchedule = rootView.findViewById(R.id.layoutSchedule);
        layoutLaunchMaps = rootView.findViewById(R.id.layoutLaunchMaps);
        moreString = rootView.findViewById(R.id.moreString);

        layoutGroups = rootView.findViewById(R.id.layoutGroups);
        listGroups = rootView.findViewById(R.id.listGroups);

        layoutRooms = rootView.findViewById(R.id.layoutRooms);
        listRooms = rootView.findViewById(R.id.listRooms);

        layoutTeachers = rootView.findViewById(R.id.layoutTeachers);
        listTeachers = rootView.findViewById(R.id.listTeachers);
        teachersNames = rootView.findViewById(R.id.teachersNames);
        groupsNames = rootView.findViewById(R.id.groupsNames);
        roomsNames = rootView.findViewById(R.id.roomsNames);

        layoutLocation.setVisibility(View.GONE);
        layoutPhone.setVisibility(View.GONE);
        layoutEmail.setVisibility(View.GONE);
        layoutWebsite.setVisibility(View.GONE);
        layoutSchedule.setVisibility(View.GONE);
        layoutLaunchMaps.setVisibility(View.GONE);

        multifunctionText = rootView.findViewById(R.id.multifunctionText);
        scheduleImg = rootView.findViewById(R.id.scheduleImg);
        helpInstance = rootView.findViewById(R.id.help_instance);

        layoutGroups.setVisibility(View.GONE);
        listGroups.setVisibility(View.GONE);

        layoutRooms.setVisibility(View.GONE);
        listRooms.setVisibility(View.GONE);

        layoutTeachers.setVisibility(View.GONE);
        listTeachers.setVisibility(View.GONE);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private boolean hasCallPermission() {
        String requiredPermission = "android.permission.CALL_PHONE";
        return getContext().checkCallingOrSelfPermission(requiredPermission) == PackageManager.PERMISSION_GRANTED;
    }

    private void setScheduleListener(LinearLayout layoutSchedule, String link) {
        layoutSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (scheduleIsClicked) {
                    orar.setVisibility(View.GONE);
                    scheduleImg.setVisibility(View.VISIBLE);
                    if (multifunctionText.getText().toString().equals("Close schedule")) {
                        multifunctionText.setText("See schedule");
                    }

                    ViewGroup.LayoutParams layoutParams = layoutSchedule.getLayoutParams();
                    layoutParams.height = 250;
                    layoutSchedule.setPadding(20, 20, 20, 20);

                    layoutSchedule.setOrientation(LinearLayout.HORIZONTAL);
                    scheduleIsClicked = false;
                    scheduleIsClicked = false;
                } else {

                    orar.setVisibility(View.VISIBLE);
                    scheduleImg.setVisibility(View.GONE);
                    if (multifunctionText.getText().toString().equals("See schedule")) {
                        multifunctionText.setText("Close schedule");
                    }

                    ViewGroup.LayoutParams layoutParams = layoutSchedule.getLayoutParams();
                    layoutParams.height = 250 + HelpMethods.getTotalHeightofListView(orar);
                    layoutSchedule.setPadding(0, 30, 0, 0);
                    layoutSchedule.setOrientation(LinearLayout.VERTICAL);
                    scheduleIsClicked = true;
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container != null) {
            container.removeAllViews();
        }
        View rootView = inflater.inflate(R.layout.fragment_instance, container, false);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Please wait ...");
        builder.setCancelable(true);
        alertDialog = builder.create();
        alertDialog.show();

        context = getContext();
        db = FirebaseFirestore.getInstance();


        findThemAll(rootView);
        searchInstance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSearchFragment();
            }
        });
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();

        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    if (user.getEmail().equals("cirimia@bitdefender.com")) {
                        startSearchFragment();
                    }
                    startDashboardFragment();
                    return true;
                }
                return false;
            }
        });

        if (user.getEmail().equals("cirimia@bitdefender.com")) {
            imageFavorite.setVisibility(View.GONE);
            helpInstance.setVisibility(View.GONE);
        }


        HelpMethods.hideActionBar(this.getActivity());
        HelpMethods.hideKeyboard(this.getActivity());


        String className = instance.getClass().getName();

        switch (className) {
            case "com.cdc.irimia.uaic_ez.StaticObjects.Group": {
                treatGroup((Group) instance);
                break;
            }
            case "com.cdc.irimia.uaic_ez.StaticObjects.Room": {
                treatRoom((Room) instance);
                break;
            }
            case "com.cdc.irimia.uaic_ez.StaticObjects.Teacher": {
                treatTeacher((Teacher) instance);
                break;
            }
            case "com.cdc.irimia.uaic_ez.StaticObjects.News": {
                treatNews((News) instance);
                break;
            }
            case "com.cdc.irimia.uaic_ez.StaticObjects.Faculty": {
                treatFaculty((Faculty) instance);
                break;
            }
            case "com.cdc.irimia.uaic_ez.StaticObjects.Building": {
                treatBuilding((Building) instance);
                break;
            }
            case "com.cdc.irimia.uaic_ez.StaticObjects.Clazz": {
                treatClazz((Clazz) instance);
                break;
            }
            default: {
                treatNull();
                break;
            }

        }

        return rootView;
    }


    private void treatNull() {
        textTitle.setText("");
        textDescription.setText("There was an error");
        setRandomImage("null");
    }

    private void setHelpOnClickListener(String ref, Object object) {
        if (user.getEmail().equals("cirimia@bitdefender.com") || !canSendData) {
            helpInstance.setVisibility(View.GONE);
            return;
        }else if(Constants.actualUser.containsKey("reputation")){
            int rep = Integer.valueOf(Constants.actualUser.get("reputation").toString());
            if(rep < 0){
                helpInstance.setVisibility(View.GONE);
                return;
            }
        }

        if (Constants.actualUser.containsKey("faculty")) {
            if (Constants.actualUser.get("faculty").toString().equals(ref)) {
                helpInstance.setVisibility(View.VISIBLE);
                helpInstance.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startHelpFragment(object);
                    }
                });
            } else {
                helpInstance.setVisibility(View.GONE);
            }
        } else {
            helpInstance.setVisibility(View.GONE);
        }
    }


    private void treatClazz(Clazz clazz) {

        setHelpOnClickListener(clazz.getRef(), clazz);

        for (Faculty faculty : Constants.facultiesList) {
            if (faculty.getRef().equals(clazz.getRef())) {
                this.faculty = faculty;
                break;
            }
        }

        db.collection("faculties").document(clazz.getRef()).collection("classes").document(clazz.getName())
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            Map<String, Object> aux = new HashMap<>();
                            if (task.getResult().exists())
                                aux = task.getResult().getData();


                            db.collection("users").document(user.getEmail()).collection("loved_classes").document(clazz.getRef() + "_" + clazz.getName()).get()
                                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                            if (task.isSuccessful()) {
                                                if (task.getResult().exists()) {
                                                    imageFavorite.setImageResource(R.drawable.ic_baseline_favorite_24px);
                                                    loveIsPressed = true;
                                                    clazz.setLoved(true);
                                                } else {
                                                    imageFavorite.setImageResource(R.drawable.ic_baseline_favorite_border_24px);
                                                    loveIsPressed = false;
                                                    clazz.setLoved(false);
                                                }
                                            }
                                        }
                                    });

                            if (aux.containsKey("link")) {
                                Orar orar = new Orar(aux.get("link").toString());
                                addDays(orar, aux);
                                clazz.setOrar(orar);
                            }

                            if (clazz.getName() != null) {
                                textTitle.setText("Class: " + clazz.getName());
                            }

                            imageFavorite.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    if (loveIsPressed) {
                                        imageFavorite.setImageResource(R.drawable.ic_baseline_favorite_border_24px);
                                        loveIsPressed = false;
                                        clazz.setLoved(false);
                                        db.collection("faculties").document(clazz.getRef()).collection("classes").document(clazz.getName()).get().addOnCompleteListener(
                                                new OnCompleteListener<DocumentSnapshot>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                        if (task.isSuccessful()) {
                                                            if (task.getResult().exists()) {
                                                                if (task.getResult().getData().containsKey("hearts")) {
                                                                    int hearts = Integer.valueOf(task.getResult().getData().get("hearts").toString());
                                                                    if (hearts >= 0)
                                                                        db.collection("faculties").document(clazz.getRef()).collection("classes").document(clazz.getName()).update("hearts", String.valueOf(hearts - 1));
                                                                } else {
                                                                    db.collection("faculties").document(clazz.getRef()).collection("classes").document(clazz.getName()).update("hearts", String.valueOf(1));
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                        );
                                        db.collection("users").document(user.getEmail()).collection("loved_classes").document(clazz.getRef() + "_" + clazz.getName()).delete();

                                    } else {
                                        imageFavorite.setImageResource(R.drawable.ic_baseline_favorite_24px);
                                        loveIsPressed = true;
                                        clazz.setLoved(true);

                                        db.collection("faculties").document(clazz.getRef()).collection("classes").document(clazz.getName()).get().addOnCompleteListener(
                                                new OnCompleteListener<DocumentSnapshot>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                        if (task.isSuccessful()) {
                                                            if (task.getResult().exists()) {
                                                                if (task.getResult().getData().containsKey("hearts")) {
                                                                    int hearts = Integer.valueOf(task.getResult().getData().get("hearts").toString());
                                                                    if (hearts >= 0)
                                                                        db.collection("faculties").document(clazz.getRef()).collection("classes").document(clazz.getName()).update("hearts", String.valueOf(hearts + 1));
                                                                } else {
                                                                    db.collection("faculties").document(clazz.getRef()).collection("classes").document(clazz.getName()).update("hearts", String.valueOf(1));
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                        );
                                        Map<String, Object> map = new HashMap<>();
                                        map.put("name", clazz.getName());
                                        map.put("faculty", clazz.getRef());
                                        db.collection("users").document(user.getEmail()).collection("loved_classes").document(clazz.getRef() + "_" + clazz.getName()).set(map);

                                    }

                                }
                            });

                            setRandomImage("class");
                            textDescription.setText(Constants.facultiesStrings.get(clazz.getRef()));

                            if (clazz.getOrar() != null) {
                                layoutSchedule.setVisibility(View.VISIBLE);
                                setScheduleListener(layoutSchedule, clazz.getOrar().getLink());
                            }
                            alertDialog.cancel();
                            fragment_instance_layout.setAlpha(1);

                        } else {
                            Toast.makeText(getContext(), "Ooups, there was an unexpected error, please try again...", Toast.LENGTH_LONG).show();
                        }
                    }
                });

    }

    private void treatGroup(Group group) {

        setHelpOnClickListener(group.getRef(), group);


        for (Faculty faculty : Constants.facultiesList) {
            if (faculty.getRef().equals(group.getRef())) {
                this.faculty = faculty;
                break;
            }
        }

        db.collection("faculties").document(group.getRef()).collection("groups").document(group.getName())
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            Map<String, Object> aux = new HashMap<>();
                            if (task.getResult().exists())
                                aux = task.getResult().getData();


                            db.collection("users").document(user.getEmail()).collection("loved_groups").document(group.getRef() + "_" + group.getName()).get()
                                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                            if (task.isSuccessful()) {
                                                if (task.getResult().exists()) {
                                                    imageFavorite.setImageResource(R.drawable.ic_baseline_favorite_24px);
                                                    loveIsPressed = true;
                                                    group.setLoved(true);
                                                } else {
                                                    imageFavorite.setImageResource(R.drawable.ic_baseline_favorite_border_24px);
                                                    loveIsPressed = false;
                                                    group.setLoved(false);
                                                }
                                            }
                                        }
                                    });

                            if (aux.containsKey("link")) {
                                Orar orar = new Orar(aux.get("link").toString());
                                addDays(orar, aux);
                                group.setOrar(orar);
                            }

                            if (group.getName() != null) {
                                textTitle.setText("Group " + group.getName());
                            }

                            imageFavorite.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    if (loveIsPressed) {
                                        imageFavorite.setImageResource(R.drawable.ic_baseline_favorite_border_24px);
                                        loveIsPressed = false;
                                        group.setLoved(false);
                                        db.collection("faculties").document(group.getRef()).collection("groups").document(group.getName()).get().addOnCompleteListener(
                                                new OnCompleteListener<DocumentSnapshot>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                        if (task.isSuccessful()) {
                                                            if (task.getResult().exists()) {
                                                                if (task.getResult().getData().containsKey("hearts")) {
                                                                    int hearts = Integer.valueOf(task.getResult().getData().get("hearts").toString());
                                                                    if (hearts >= 0)
                                                                        db.collection("faculties").document(group.getRef()).collection("groups").document(group.getName()).update("hearts", String.valueOf(hearts - 1));
                                                                } else {
                                                                    db.collection("faculties").document(group.getRef()).collection("groups").document(group.getName()).update("hearts", String.valueOf(1));
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                        );
                                        db.collection("users").document(user.getEmail()).collection("loved_groups").document(group.getRef() + "_" + group.getName()).delete();

                                    } else {
                                        imageFavorite.setImageResource(R.drawable.ic_baseline_favorite_24px);
                                        loveIsPressed = true;
                                        group.setLoved(true);

                                        if (Constants.groupsPriority.containsKey(group.getName())) {
                                            Constants.groupsPriority.remove(group.getName());
                                        }

                                        db.collection("faculties").document(group.getRef()).collection("groups").document(group.getName()).get().addOnCompleteListener(
                                                new OnCompleteListener<DocumentSnapshot>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                        if (task.isSuccessful()) {
                                                            if (task.getResult().exists()) {
                                                                if (task.getResult().getData().containsKey("hearts")) {
                                                                    int hearts = Integer.valueOf(task.getResult().getData().get("hearts").toString());
                                                                    if (hearts >= 0)
                                                                        db.collection("faculties").document(group.getRef()).collection("groups").document(group.getName()).update("hearts", String.valueOf(hearts + 1));
                                                                } else {
                                                                    db.collection("faculties").document(group.getRef()).collection("groups").document(group.getName()).update("hearts", String.valueOf(1));
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                        );
                                        Map<String, Object> map = new HashMap<>();
                                        map.put("name", group.getName());
                                        map.put("faculty", group.getRef());
                                        db.collection("users").document(user.getEmail()).collection("loved_groups").document(group.getRef() + "_" + group.getName()).set(map);

                                    }

                                }
                            });

                            setRandomImage("group");
                            textDescription.setText(Constants.facultiesStrings.get(group.getRef()));

                            if (group.getOrar() != null) {
                                layoutSchedule.setVisibility(View.VISIBLE);
                                setScheduleListener(layoutSchedule, group.getOrar().getLink());
                            }
                            alertDialog.cancel();
                            fragment_instance_layout.setAlpha(1);

                        } else {
                            Toast.makeText(getContext(), "Ooups, there was an unexpected error, please try again...", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private void addDays(Orar orar, Map<String, Object> aux) {
        if (aux.containsKey("type")) orar.setType(aux.get("type").toString());

        if (aux.containsKey("Luni")) orar.addDay("Luni", aux.get("Luni"));
        if (aux.containsKey("Marti")) orar.addDay("Marti", aux.get("Marti"));
        if (aux.containsKey("Miercuri")) orar.addDay("Miercuri", aux.get("Miercuri"));
        if (aux.containsKey("Joi")) orar.addDay("Joi", aux.get("Joi"));
        if (aux.containsKey("Vineri")) orar.addDay("Vineri", aux.get("Vineri"));
        if (aux.containsKey("Sambata")) orar.addDay("Sambata", aux.get("Sambata"));
        if (aux.containsKey("Duminica")) orar.addDay("Duminica", aux.get("Duminica"));
        printOrar(orar);
    }

    private void printOrar(Orar or) {
        checkIfIsLive(or);
        if(or.getDayList() == null || or.getDayList().isEmpty()){
            orar.setVisibility(View.GONE);
            return;
        }
        adapterOrar = new OrarAdapter(this.fragment, or.getDayList(), context);

        orar.setAdapter(adapterOrar);
        orar.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // AFISEZA O LISTA CU TOATE OBIECTELE DIN ORAR SI PE CARE APASA SA SE CREEZE O INSTANTA NOUA

            }
        });

    }

    private Day setMyDay(List<Day> dayList, String dayName) {
        for (Day d : dayList) {
            if (d.getName().equals(dayName)) {
                return d;
            }
        }
        return null;
    }

    private boolean isValidHour(String min, String max) {
        String[] parts;
        Calendar currentCal = Calendar.getInstance();

        parts = min.split(":");
        if (parts.length < 2) return false;
        Calendar minCalendar = Calendar.getInstance();
        minCalendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(parts[0]));
        minCalendar.set(Calendar.MINUTE, Integer.parseInt(parts[1]));

        parts = max.split(":");
        if (parts.length < 2) return false;
        Calendar maxCalendar = Calendar.getInstance();
        maxCalendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(parts[0]));
        maxCalendar.set(Calendar.MINUTE, Integer.parseInt(parts[1]));

        return currentCal.before(maxCalendar) && minCalendar.before(currentCal);
    }

    private void checkIfIsLive(Orar or) {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        Day myDay = null;

        switch (day) {
            case Calendar.SUNDAY:
                myDay = setMyDay(or.getDayList(), "Duminica");
                break;
            case Calendar.MONDAY:
                myDay = setMyDay(or.getDayList(), "Luni");
                break;
            case Calendar.TUESDAY:
                myDay = setMyDay(or.getDayList(), "Marti");
                break;
            case Calendar.WEDNESDAY:
                myDay = setMyDay(or.getDayList(), "Miercuri");
                break;
            case Calendar.THURSDAY:
                myDay = setMyDay(or.getDayList(), "Joi");
                break;
            case Calendar.FRIDAY:
                myDay = setMyDay(or.getDayList(), "Vineri");
                break;
            case Calendar.SATURDAY:
                myDay = setMyDay(or.getDayList(), "Sambata");
                break;
        }

        if (myDay != null) {
            for (Ora ora : myDay.getOraList()) {
                if (isValidHour(ora.getDe_la(), ora.getPana_la())) {
                    if (myDay.getType() != null) {
                        String type = myDay.getType();
                        Spanned text = null;
                        switch (type) {
                            case "room": {
                                if (ora.getTip().equals("Seminar") && ora.getProfesori().size() > 0) {
                                    text = Html.fromHtml("<p style=\"color:red;\"><strong>LIVE:</strong> " + ora.getDisciplina() + "</p><small>Seminar - " + ora.getProfesori().get(0) + "</small>");
                                } else if (ora.getProfesori().size() > 0) {
                                    text = Html.fromHtml("<p style=\"color:red;\"><strong>LIVE:</strong>  Curs - " + ora.getProfesori().get(0) + "</p><small>" + ora.getDisciplina() + "</small>");
                                }
                                break;
                            }
                            case "prof": {

                                if (ora.getTip().equals("Seminar") && ora.getStudenti().size() > 0) {
                                    text = Html.fromHtml("<p style=\"color:red;\"><strong>LIVE:</strong> " + ora.getSala() + " - " + ora.getDisciplina() + "</p><small>" + ora.getTip() + " - " + ora.getStudenti().get(0) + "</small>");
                                } else {
                                    text = Html.fromHtml("<p style=\"color:red;\"><strong>LIVE:</strong> " + ora.getSala() + "</p><small>" + ora.getTip() + " - " + ora.getDisciplina() + "</small>");
                                }
                                break;
                            }
                            case "class": {
                                if (ora.getTip().equals("Seminar") && ora.getProfesori().size() > 0 && ora.getStudenti().size() > 0) {
                                    text = Html.fromHtml("<p style=\"color:red;\"><strong>LIVE:</strong> " + ora.getSala() + " - " + ora.getStudenti().get(0) + "</p><small>" + ora.getTip() + " - " + ora.getProfesori().get(0) + "</small>");
                                } else {
                                    text = Html.fromHtml("<p style=\"color:red;\"><strong>LIVE:</strong> " + ora.getSala() + "</p><small>" + ora.getTip() + " - " + ora.getDisciplina() + "</small>");
                                }
                                break;
                            }
                            case "group": {
                                if (ora.getTip().equals("Seminar") && ora.getProfesori().size() > 0) {
                                    text = Html.fromHtml("<p style=\"color:red;\"><strong>LIVE:</strong> " + ora.getSala() + " - " + ora.getDisciplina() + "</p><small>" + ora.getTip() + " - " + ora.getProfesori().get(0) + "</small>");
                                } else {
                                    text = Html.fromHtml("<p style=\"color:red;\"><strong>LIVE:</strong> " + ora.getSala() + "</p><small>" + ora.getTip() + " - " + ora.getDisciplina() + "</small>");
                                }
                                break;
                            }
                        }
                        if (text != null) {
                            multifunctionText.setText(text);
                        }
                    }
                    break;
                }
            }
        }
    }

    private void treatRoom(Room room) {

        setHelpOnClickListener(room.getRef(), room);


        for (Faculty faculty : Constants.facultiesList) {
            if (faculty.getRef().equals(room.getRef())) {
                this.faculty = faculty;
                break;
            }
        }

        db.collection("faculties").document(room.getRef()).collection("rooms").document(room.getName())
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            Map<String, Object> aux = new HashMap<>();
                            if (task.getResult().exists())
                                aux = task.getResult().getData();

                            db.collection("users").document(user.getEmail()).collection("loved_rooms").document(room.getRef() + "_" + room.getName().replace(" ", "_")).get()
                                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                            if (task.isSuccessful()) {
                                                if (task.getResult().exists()) {
                                                    imageFavorite.setImageResource(R.drawable.ic_baseline_favorite_24px);
                                                    loveIsPressed = true;
                                                    room.setLoved(true);
                                                } else {
                                                    imageFavorite.setImageResource(R.drawable.ic_baseline_favorite_border_24px);
                                                    loveIsPressed = false;
                                                    room.setLoved(true);
                                                }
                                            }
                                        }
                                    });

                            if (aux.containsKey("link")) {
                                Orar orar = new Orar(aux.get("link").toString());
                                addDays(orar, aux);
                                room.setOrar(orar);
                            }


                            if (aux.containsKey("floor"))
                                room.setFloor(aux.get("floor").toString());


                            if (room.getName() != null) {
                                textTitle.setText(room.getName());
                            }


                            imageFavorite.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    if (loveIsPressed) {
                                        imageFavorite.setImageResource(R.drawable.ic_baseline_favorite_border_24px);
                                        loveIsPressed = false;
                                        room.setLoved(false);
                                        db.collection("faculties").document(room.getRef()).collection("rooms").document(room.getName()).get().addOnCompleteListener(
                                                new OnCompleteListener<DocumentSnapshot>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                        if (task.isSuccessful()) {
                                                            if (task.getResult().exists()) {
                                                                if (task.getResult().getData().containsKey("hearts")) {
                                                                    int hearts = Integer.valueOf(task.getResult().getData().get("hearts").toString());
                                                                    if (hearts >= 0)
                                                                        db.collection("faculties").document(room.getRef()).collection("rooms").document(room.getName()).update("hearts", String.valueOf(hearts - 1));
                                                                } else {
                                                                    db.collection("faculties").document(room.getRef()).collection("rooms").document(room.getName()).update("hearts", String.valueOf(1));
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                        );
                                        db.collection("users").document(user.getEmail()).collection("loved_rooms").document(room.getRef() + "_" + room.getName().replace(" ", "_")).delete();

                                    } else {
                                        imageFavorite.setImageResource(R.drawable.ic_baseline_favorite_24px);
                                        loveIsPressed = true;
                                        room.setLoved(true);

                                        if (Constants.roomsPriority.containsKey(room.getName())) {
                                            Constants.roomsPriority.remove(room.getName());
                                        }
                                        Map<String, Object> map = new HashMap<>();
                                        map.put("name", room.getName());
                                        map.put("faculty", room.getRef());
                                        db.collection("faculties").document(room.getRef()).collection("rooms").document(room.getName()).get().addOnCompleteListener(
                                                new OnCompleteListener<DocumentSnapshot>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                        if (task.isSuccessful()) {
                                                            if (task.getResult().exists()) {
                                                                if (task.getResult().getData().containsKey("hearts")) {
                                                                    int hearts = Integer.valueOf(task.getResult().getData().get("hearts").toString());
                                                                    if (hearts >= 0)
                                                                        db.collection("faculties").document(room.getRef()).collection("rooms").document(room.getName()).update("hearts", String.valueOf(hearts + 1));
                                                                } else {
                                                                    db.collection("faculties").document(room.getRef()).collection("rooms").document(room.getName()).update("hearts", String.valueOf(1));
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                        );
                                        db.collection("users").document(user.getEmail()).collection("loved_rooms").document(room.getRef() + "_" + room.getName().replace(" ", "_")).set(map);
                                    }

                                }
                            });

                            setRandomImage("room");
                            textDescription.setText(Constants.facultiesStrings.get(room.getRef()));

                            if (room.getEmail() != null) {
                                layoutEmail.setVisibility(View.VISIBLE);
                                textEmail.setText(room.getEmail());
                                layoutEmail.setOnClickListener(emailClickListener);
                            }

                            if (room.getFloor() != null) {
                                layoutLocation.setVisibility(View.VISIBLE);
                                int floor = Integer.valueOf(room.getFloor());
                                String location = "";
                                location = getLocationFloor(floor) + location + room.getName();
                                layoutLocation.setOnClickListener(mapsClickListener);
                                textLocation.setText(location);
                            } else {

                                layoutLocation.setVisibility(View.VISIBLE);
                                textLocation.setText(room.getName() + ", " + faculty.getName());
                                layoutLocation.setOnClickListener(mapsClickListener);

                            }

                            if (room.getOrar() != null) {
                                layoutSchedule.setVisibility(View.VISIBLE);
                                setScheduleListener(layoutSchedule, room.getOrar().getLink());
                            }

                            if (room.getPhoneNo() != null) {
                                layoutPhone.setVisibility(View.VISIBLE);
                                textPhone.setText(room.getPhoneNo());
                                layoutPhone.setOnClickListener(phoneClickListener);
                            }

                            alertDialog.cancel();
                            fragment_instance_layout.setAlpha(1);
                        } else {
                            Toast.makeText(getContext(), "Ooups, there was an unexpected error, please try again...", Toast.LENGTH_LONG).show();
                        }
                    }
                });

    }

    private String getLocationFloor(int floor) {
        String location = "";
        switch (floor) {
            case -1: {
                location = "Basement floor, ";
                break;
            }
            case 0: {
                location = "Ground floor, ";
                break;
            }
            case 1: {
                location = "First floor, ";
                break;
            }
            case 2: {
                location = "Second floor, ";
                break;
            }
            case 3: {
                location = "Third floor, ";
                break;
            }
            case 4: {
                location = "The fourth  floor, ";
                break;
            }
            case 5: {
                location = "The fifth floor, ";
                break;
            }
            case 6: {
                location = "The sixth  floor, ";
                break;
            }
            case 7: {
                location = "The seventh  floor, ";
                break;
            }
        }
        return location;
    }

    private void treatTeacher(Teacher teacher) {

        setHelpOnClickListener(teacher.getRef(), teacher);


        for (Faculty faculty : Constants.facultiesList) {
            if (faculty.getRef().equals(teacher.getRef())) {
                this.faculty = faculty;
                break;
            }
        }

        db.collection("faculties").document(teacher.getRef()).collection("teachers").document(teacher.getName())
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            Map<String, Object> aux = new HashMap<>();
                            if (task.getResult().exists())
                                aux = task.getResult().getData();

                            db.collection("users").document(user.getEmail()).collection("loved_teachers").document(teacher.getRef() + "_" + teacher.getName().replace(" ", "_")).get()
                                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                            if (task.isSuccessful()) {
                                                if (task.getResult().exists()) {
                                                    imageFavorite.setImageResource(R.drawable.ic_baseline_favorite_24px);
                                                    loveIsPressed = true;
                                                    teacher.setLoved(true);
                                                } else {
                                                    imageFavorite.setImageResource(R.drawable.ic_baseline_favorite_border_24px);
                                                    loveIsPressed = false;
                                                    teacher.setLoved(false);
                                                }
                                            }
                                        }
                                    });

                            if (aux.containsKey("email"))
                                teacher.setEmail(aux.get("email").toString());

                            if (aux.containsKey("name"))
                                teacher.setName(aux.get("name").toString());

                            if (aux.containsKey("phoneNo"))
                                teacher.setPhoneNo(aux.get("phoneNo").toString());

                            if (aux.containsKey("room"))
                                teacher.setRoom(new Room(aux.get("room").toString(), teacher.getRef()));

                            if (aux.containsKey("link")) {
                                Orar orar = new Orar(aux.get("link").toString());
                                addDays(orar, aux);
                                teacher.setOrar(orar);
                            }


                            if (teacher.getName() != null) {
                                textTitle.setText(teacher.getName());
                            }


                            textDescription.setText("");
                            setRandomImage("teacher");

                            if (teacher.getEmail() != null) {
                                layoutEmail.setVisibility(View.VISIBLE);
                                textEmail.setText(teacher.getEmail());
                                layoutEmail.setOnClickListener(emailClickListener);
                            }

                            imageFavorite.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    if (loveIsPressed) {
                                        imageFavorite.setImageResource(R.drawable.ic_baseline_favorite_border_24px);
                                        loveIsPressed = false;
                                        teacher.setLoved(false);
                                        db.collection("faculties").document(teacher.getRef()).collection("teachers").document(teacher.getName()).get().addOnCompleteListener(
                                                new OnCompleteListener<DocumentSnapshot>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                        if (task.isSuccessful()) {
                                                            if (task.getResult().exists()) {
                                                                if (task.getResult().getData().containsKey("hearts")) {
                                                                    int hearts = Integer.valueOf(task.getResult().getData().get("hearts").toString());
                                                                    if (hearts >= 0)
                                                                        db.collection("faculties").document(teacher.getRef()).collection("teachers").document(teacher.getName()).update("hearts", String.valueOf(hearts - 1));
                                                                } else {
                                                                    db.collection("faculties").document(teacher.getRef()).collection("teachers").document(teacher.getName()).update("hearts", String.valueOf(1));
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                        );
                                        db.collection("users").document(user.getEmail()).collection("loved_teachers").document(teacher.getRef() + "_" + teacher.getName().replace(" ", "_")).delete();

                                    } else {
                                        imageFavorite.setImageResource(R.drawable.ic_baseline_favorite_24px);
                                        loveIsPressed = true;
                                        teacher.setLoved(true);

                                        if (Constants.teachersPriority.containsKey(teacher.getName())) {
                                            Constants.teachersPriority.remove(teacher.getName());
                                        }

                                        Map<String, Object> map = new HashMap<>();
                                        map.put("name", teacher.getName());
                                        map.put("faculty", teacher.getRef());
                                        db.collection("faculties").document(teacher.getRef()).collection("teachers").document(teacher.getName()).get().addOnCompleteListener(
                                                new OnCompleteListener<DocumentSnapshot>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                        if (task.isSuccessful()) {
                                                            if (task.getResult().exists()) {
                                                                if (task.getResult().getData().containsKey("hearts")) {
                                                                    int hearts = Integer.valueOf(task.getResult().getData().get("hearts").toString());
                                                                    if (hearts >= 0)
                                                                        db.collection("faculties").document(teacher.getRef()).collection("teachers").document(teacher.getName()).update("hearts", String.valueOf(hearts + 1));
                                                                } else {
                                                                    db.collection("faculties").document(teacher.getRef()).collection("teachers").document(teacher.getName()).update("hearts", String.valueOf(1));
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                        );
                                        db.collection("users").document(user.getEmail()).collection("loved_teachers").document(teacher.getRef() + "_" + teacher.getName().replace(" ", "_")).set(map);
                                        alertDialog.cancel();
                                        fragment_instance_layout.setAlpha(1);
                                    }

                                }
                            });

                            if (teacher.getOrar() != null) {
                                layoutSchedule.setVisibility(View.VISIBLE);
                                setScheduleListener(layoutSchedule, teacher.getOrar().getLink());

                            }

                            if (teacher.getPhoneNo() != null) {
                                layoutPhone.setVisibility(View.VISIBLE);
                                textPhone.setText(teacher.getPhoneNo());
                                layoutPhone.setOnClickListener(phoneClickListener);
                            }

                            if (teacher.getRoom() != null) {
                                layoutRooms.setVisibility(View.VISIBLE);
                                List<String> rooms = new ArrayList<>();
                                rooms.add(teacher.getRoom().getName());
                                ArrayAdapter<String> itemsAdapter =
                                        new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, rooms);
                                listRooms.setAdapter(itemsAdapter);
                                listRooms.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        startInstanceFragment(teacher.getRoom());
                                    }
                                });

                                layoutRooms.setOnClickListener(roomsClickListener);

                                layoutLocation.setVisibility(View.VISIBLE);
                                textLocation.setText(teacher.getRoom().getName() + ", " + faculty.getName());
                                layoutLocation.setOnClickListener(mapsClickListener);


                            }

                            alertDialog.cancel();
                            fragment_instance_layout.setAlpha(1);

                        } else {
                            Toast.makeText(getContext(), "Ooups, there was an unexpected error, please try again...", Toast.LENGTH_LONG).show();
                        }
                    }
                });

    }

    private void treatNews(News news) {
        if (news.getTitle() != null) {
            textTitle.setText(news.getTitle());
        }
        setRandomImage("news");
        if (news.getDescription() != null) {
            textDescription.setText(news.getDescription());
        }

        imageFavorite.setVisibility(View.GONE);

        if (news.getSomeLink() != null && !news.getSomeLink().equals(" ")) {
            layoutWebsite.setVisibility(View.VISIBLE);
            textWebsite.setText(news.getSomeLink());
            layoutWebsite.setOnClickListener(webClickListener);
        }

        if (news.getPictureLink() != null) {
            new Thread() {
                public void run() {
                    new DownloadImage(imageMiddleRound).execute(news.getPictureLink());
                }
            }.start();
        }

        if (news.getReadMoreString() != null) {
            layoutLaunchMaps.setVisibility(View.VISIBLE);
            moreString.setText(news.getReadMoreString());
        }

        alertDialog.cancel();
        fragment_instance_layout.setAlpha(1);
    }

    private void treatBuilding(Building building) {

        if (building.getDescription() != null) {
            textDescription.setText(building.getDescription());
        }
        setRandomImage("building");
        if (building.getOrar() != null) {
            layoutSchedule.setVisibility(View.VISIBLE);
            layoutSchedule.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //LOAD LINK
                }
            });
        }

        if (building.getPhoneNo() != null) {
            layoutPhone.setVisibility(View.VISIBLE);
            textPhone.setText(building.getPhoneNo());
            layoutPhone.setOnClickListener(phoneClickListener);
        }
        alertDialog.cancel();
        fragment_instance_layout.setAlpha(1);
    }

    private void treatFaculty(Faculty faculty) {

        this.faculty = faculty;

        db.collection("users").document(user.getEmail()).collection("loved_faculties")
                .document(faculty.getRef() + "_" + faculty.getName().replace(" ", "_")).get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            if (task.getResult().exists()) {
                                imageFavorite.setImageResource(R.drawable.ic_baseline_favorite_24px);
                                loveIsPressed = true;
                                faculty.setLoved(true);
                            } else {
                                imageFavorite.setImageResource(R.drawable.ic_baseline_favorite_border_24px);
                                loveIsPressed = false;
                                faculty.setLoved(false);
                            }
                        }
                    }
                });


        if (faculty.getName() != null) {
            textTitle.setText(faculty.getName());
        }
        setRandomImage("faculty");
        if (faculty.getDescription() != null) {
            textDescription.setText(faculty.getDescription());
        } else {
            textDescription.setText("");
        }

        if (faculty.getPhoneNo() != null) {
            layoutPhone.setVisibility(View.VISIBLE);
            textPhone.setText(faculty.getPhoneNo());
            layoutPhone.setOnClickListener(phoneClickListener);
        }

        if (faculty.getEmail() != null) {
            layoutEmail.setVisibility(View.VISIBLE);
            textEmail.setText(faculty.getEmail());
            layoutEmail.setOnClickListener(emailClickListener);
        }

        if (faculty.getWebsite() != null) {
            layoutWebsite.setVisibility(View.VISIBLE);
            textWebsite.setText(HelpMethods.URLdecode(faculty.getWebsite()));
            layoutWebsite.setOnClickListener(webClickListener);
        }

        if (faculty.getLocation() != null) {
            layoutLocation.setVisibility(View.VISIBLE);
            textLocation.setText(faculty.getLocation());
            layoutLocation.setOnClickListener(mapsClickListener);
        }

        if (faculty.getRooms() != null) {
            layoutRooms.setVisibility(View.VISIBLE);
            List<String> roomsString = new ArrayList<>();
            for (Room room : faculty.getRooms()) {
                roomsString.add(room.getName());
            }
            ArrayAdapter<String> roomsAdapter =
                    new ArrayAdapter<String>(this.getContext(), android.R.layout.simple_list_item_1, roomsString);
            listRooms.setAdapter(roomsAdapter);
            listRooms.setOnItemClickListener(roomsItemListener);
            layoutRooms.setOnClickListener(roomsClickListener);
        }

        if (faculty.getGroups() != null) {
            layoutGroups.setVisibility(View.VISIBLE);
            List<String> groupsString = new ArrayList<>();
            for (Group group : faculty.getGroups()) {
                groupsString.add(group.getName());
            }
            ArrayAdapter<String> groupsAdapter =
                    new ArrayAdapter<String>(this.getContext(), android.R.layout.simple_list_item_1, groupsString);
            listGroups.setAdapter(groupsAdapter);
            listGroups.setOnItemClickListener(groupsItemListener);
            layoutGroups.setOnClickListener(groupsClickListener);
        }

        if (faculty.getTeachers() != null) {
            layoutTeachers.setVisibility(View.VISIBLE);
            List<String> teachersString = new ArrayList<>();
            for (Teacher teacher : faculty.getTeachers()) {
                teachersString.add(teacher.getName());
            }
            ArrayAdapter<String> teachersAdapter =
                    new ArrayAdapter<String>(this.getContext(), android.R.layout.simple_list_item_1, teachersString);
            listTeachers.setAdapter(teachersAdapter);
            listTeachers.setOnItemClickListener(teachersItemListener);
            layoutTeachers.setOnClickListener(teachersClickListener);
        }

        alertDialog.cancel();
        fragment_instance_layout.setAlpha(1);

        imageFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (loveIsPressed) {
                    imageFavorite.setImageResource(R.drawable.ic_baseline_favorite_border_24px);
                    loveIsPressed = false;
                    faculty.setLoved(false);
                    db.collection("faculties").document(faculty.getRef()).get().addOnCompleteListener(
                            new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    if (task.isSuccessful()) {
                                        if (task.getResult().exists()) {
                                            if (task.getResult().getData().containsKey("hearts")) {
                                                int hearts = Integer.valueOf(task.getResult().getData().get("hearts").toString());
                                                if (hearts > 0)
                                                    db.collection("faculties").document(faculty.getRef()).update("hearts", String.valueOf(hearts - 1));
                                            } else {
                                                db.collection("faculties").document(faculty.getRef()).update("hearts", String.valueOf(0));
                                            }
                                        }
                                    }
                                }
                            }
                    );
                    db.collection("users").document(user.getEmail()).collection("loved_faculties").document(faculty.getRef() + "_" + faculty.getName().replace(" ", "_")).delete();

                } else {
                    imageFavorite.setImageResource(R.drawable.ic_baseline_favorite_24px);
                    loveIsPressed = true;
                    faculty.setLoved(true);
                    Map<String, Object> map = new HashMap<>();
                    map.put("name", faculty.getName());
                    map.put("faculty", faculty.getRef());
                    db.collection("faculties").document(faculty.getRef()).get().addOnCompleteListener(
                            new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    if (task.isSuccessful()) {
                                        if (task.getResult().exists()) {
                                            if (task.getResult().getData().containsKey("hearts")) {
                                                int hearts = Integer.valueOf(task.getResult().getData().get("hearts").toString());
                                                if (hearts > 0)
                                                    db.collection("faculties").document(faculty.getRef()).update("hearts", String.valueOf(hearts + 1));
                                            } else {
                                                db.collection("faculties").document(faculty.getRef()).update("hearts", String.valueOf(1));
                                            }
                                        }
                                    }
                                }
                            }
                    );
                    db.collection("users").document(user.getEmail()).collection("loved_faculties").document(faculty.getRef() + "_" + faculty.getName().replace(" ", "_")).set(map);

                }

            }
        });

    }

    private void startInstanceFragment(Object instance) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, InstanceFragment.newInstance(instance));
        transaction.commit();
    }

    private void startHelpFragment(Object instance) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, HelpFragment.newInstance(instance));
        transaction.commit();
    }

    private void startSearchFragment() {
        startDashboardFragment();
        ((MainActivity) getActivity()).imageButton.performClick();

    }

    private void startDashboardFragment() {

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, DashboardFragment.newInstance());
        transaction.commit();

    }

    @NeedsPermission(Manifest.permission.CALL_PHONE)
    void needPermission() {
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        InstanceFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @OnPermissionDenied(Manifest.permission.CALL_PHONE)
    void noCallPermission() {
        Toast.makeText(((InstanceFragment) instance).getContext(), "I cannot call without permissions, sorry", Toast.LENGTH_LONG).show();
    }

    private class DownloadImage extends AsyncTask<String, Void, Bitmap> {
        private ImageView _image;

        public DownloadImage(ImageView image) {
            _image = image;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            String url = params[0];
            Bitmap bitmap = null;

            try {
                InputStream in = new java.net.URL(url).openStream();
                bitmap = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
            }

            return bitmap;
        }

        protected void onPostExecute(Bitmap result) {
            if (result != null)
                _image.setImageBitmap(result);
        }
    }
}
