package com.cdc.irimia.uaic_ez.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.cdc.irimia.uaic_ez.Activities.MainActivity;
import com.cdc.irimia.uaic_ez.Common.Constants;
import com.cdc.irimia.uaic_ez.Common.HelpMethods;
import com.cdc.irimia.uaic_ez.R;
import com.cdc.irimia.uaic_ez.StaticObjects.News;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class DashboardFragment extends Fragment {

    private static CustomAdapter adapter;
    ArrayList<News> dataModels;
    ListView listView;
    private RecyclerView.ViewHolder noNewsHolder;
    private TextView textView1;
    private TextView textView2;
    private ImageView imageView1;
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private DashboardFragment dashboardFragment;

    public static Fragment newInstance() {
        DashboardFragment fragment = new DashboardFragment();
        return fragment;
    }

    public void refresh() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.detach(this).attach(this).commit();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        dashboardFragment = this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container != null) {
            container.removeAllViews();
        }
        HelpMethods.showActionBar(this.getActivity());


        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();


        final View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);


        if (user.getEmail().equals("cirimia@bitdefender.com")) {
            ((MainActivity) getActivity()).imageButton.performClick();
        }

        listView = rootView.findViewById(R.id.list);
        imageView1 = rootView.findViewById(R.id.dashboard_faceImg);
        textView1 = rootView.findViewById(R.id.dashboard_text1);
        textView2 = rootView.findViewById(R.id.dashboard_text2);


        if (Constants.newsList.isEmpty()) {
            imageView1.setVisibility(View.VISIBLE);
            textView1.setVisibility(View.VISIBLE);
            textView2.setVisibility(View.VISIBLE);
            listView.setVisibility(View.INVISIBLE);

            textView2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    db.collection("faculties").document(Constants.actualUser.get("faculty").toString()).collection("news").get().addOnCompleteListener(
                            new OnCompleteListener<QuerySnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                    if (task.isSuccessful()) {
                                        if (!task.getResult().isEmpty()) {
                                            for (DocumentSnapshot documentSnapshot : task.getResult().getDocuments()) {
                                                Constants.addNews(documentSnapshot.getId(), documentSnapshot.getData());
                                                db.collection("users").document(user.getEmail()).collection("news").document(documentSnapshot.getId()).set(documentSnapshot.getData());
                                            }
                                        }
                                        imageView1.setVisibility(View.INVISIBLE);
                                        textView1.setVisibility(View.INVISIBLE);
                                        textView2.setVisibility(View.INVISIBLE);
                                        listView.setVisibility(View.VISIBLE);

                                        dataModels = new ArrayList<>(Constants.newsList);

                                        adapter = new CustomAdapter(dashboardFragment, dataModels, rootView.getContext());

                                        listView.setAdapter(adapter);
                                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                                News dataModel = dataModels.get(position);
                                                startInstanceFragment(dataModel);

                                                // Snackbar.make(view, dataModel.getTitle()+"\n"+dataModel.getDescription()+" API: "+dataModel.getImportance(), Snackbar.LENGTH_LONG)
                                                //       .setAction("No action", null).show();
                                            }
                                        });

                                    }
                                }
                            }
                    );

                }
            });

        } else {

            imageView1.setVisibility(View.INVISIBLE);
            textView1.setVisibility(View.INVISIBLE);
            textView2.setVisibility(View.INVISIBLE);
            listView.setVisibility(View.VISIBLE);

            dataModels = new ArrayList<>(Constants.newsList);

            adapter = new CustomAdapter(this, dataModels, rootView.getContext());

            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    News dataModel = dataModels.get(position);
                    startInstanceFragment(dataModel);

                    // Snackbar.make(view, dataModel.getTitle()+"\n"+dataModel.getDescription()+" API: "+dataModel.getImportance(), Snackbar.LENGTH_LONG)
                    //       .setAction("No action", null).show();
                }
            });
        }

        return rootView;
    }


    private void startInstanceFragment(Object instance) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, InstanceFragment.newInstance(instance));
        transaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }


}
