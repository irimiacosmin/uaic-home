package com.cdc.irimia.uaic_ez.Common;

import android.support.annotation.NonNull;
import android.util.Log;

import com.cdc.irimia.uaic_ez.StaticObjects.Clazz;
import com.cdc.irimia.uaic_ez.StaticObjects.Faculty;
import com.cdc.irimia.uaic_ez.StaticObjects.Group;
import com.cdc.irimia.uaic_ez.StaticObjects.News;
import com.cdc.irimia.uaic_ez.StaticObjects.Room;
import com.cdc.irimia.uaic_ez.StaticObjects.Teacher;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Constants {

    public static Map<String, Map<String, Object>> faculties = new HashMap<>();
    public static Map<String, Map<String, Object>> news = new HashMap<>();
    public static Map<String, String> facultiesStrings = new HashMap<>();
    public static Map<String, String> suggestions = new HashMap<>();
    public static Map<String, Object> actualUser = new HashMap<>();
    public static Map<String, Object> teachers = new HashMap<>();
    public static Map<String, Object> groups = new HashMap<>();
    public static Map<String, Object> rooms = new HashMap<>();
    public static Map<String, Object> classes = new HashMap<>();

    public static Map<String, Integer> groupsPriority = new HashMap<>();
    public static Map<String, Integer> teachersPriority = new HashMap<>();
    public static Map<String, Integer> roomsPriority = new HashMap<>();
    public static Map<String, Integer> classesPriority = new HashMap<>();

    public static List<Faculty> facultiesList = new ArrayList<Faculty>();
    public static List<Group> groupsList = new ArrayList<Group>();
    public static List<Room> roomList = new ArrayList<Room>();
    public static List<Teacher> teacherList = new ArrayList<Teacher>();
    public static List<News> newsList = new ArrayList<News>();
    public static List<Clazz> classList = new ArrayList<Clazz>();


    private static FirebaseFirestore db;
    private static FirebaseAuth mAuth;
    private static FirebaseUser user;

    public static void setUser(Map<String, Object> map) {
        actualUser = map;

        db = FirebaseFirestore.getInstance();

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();

        List<String> aux;

        for (String key : facultiesStrings.keySet()) {
            facultiesList.add(new Faculty(key, facultiesStrings.get(key)));
        }


        Map<String, Object> auxMap = faculties.get(actualUser.get("faculty"));


        for (Faculty faculty : facultiesList) {
            if (faculty.getRef().equals(actualUser.get("faculty"))) {

                if (auxMap.containsKey("email"))
                    faculty.setEmail(auxMap.get("email").toString());

                if (auxMap.containsKey("phoneNo"))
                    faculty.setPhoneNo(auxMap.get("phoneNo").toString());

                if (auxMap.containsKey("website"))
                    faculty.setWebsite(auxMap.get("website").toString());

                if (auxMap.containsKey("virtual_tour"))
                    faculty.setVirtualTour(auxMap.get("virtual_tour").toString());

                if (auxMap.containsKey("photoLink"))
                    faculty.setPhotoLink(auxMap.get("photoLink").toString());

                if (auxMap.containsKey("hearts"))
                    faculty.setHearts(Integer.valueOf(auxMap.get("hearts").toString()));

                if (auxMap.containsKey("adress")) {
                    HashMap<String, Object> adress = new HashMap<>();
                    adress = (HashMap<String, Object>) auxMap.get("adress");

                    if (adress.containsKey("full")) {
                        faculty.setLocation(adress.get("full").toString());
                    }

                    if (adress.containsKey("location")) {
                        faculty.setGeoPoint((GeoPoint) adress.get("location"));
                    }
                }

                break;
            }
        }


        aux = new ArrayList<>((Collection<? extends String>) Constants.groups.get(actualUser.get("faculty")));
        for (String name : aux) {
            groupsList.add(new Group(name, actualUser.get("faculty").toString()));
        }


        for (String key : teachers.keySet()) {
            aux = new ArrayList<>((Collection<? extends String>) Constants.teachers.get(key));
            for (String name : aux) {
                Teacher teacher = new Teacher(name, key);
                teacherList.add(teacher);
                for (Faculty faculty : facultiesList) {
                    if (faculty.getRef().equals(key)) {
                        faculty.addTeacher(teacher);
                        break;
                    }
                }
            }
        }

        for (String key : rooms.keySet()) {
            aux = new ArrayList<>((Collection<? extends String>) Constants.rooms.get(key));
            for (String name : aux) {
                Room room = new Room(name, key);
                roomList.add(room);
                for (Faculty faculty : facultiesList) {
                    if (faculty.getRef().equals(key)) {
                        faculty.addRoom(room);
                        break;
                    }
                }
            }
        }
        for (String key : classes.keySet()) {
            aux = new ArrayList<>((Collection<? extends String>) Constants.classes.get(key));
            for (String name : aux) {
                Clazz clazz = new Clazz(name, key);
                classList.add(clazz);
                for (Faculty faculty : facultiesList) {
                    if (faculty.getRef().equals(key)) {
                        faculty.addClazz(clazz);
                        break;
                    }
                }
            }
        }


        for (String key : groups.keySet()) {
            aux = new ArrayList<>((Collection<? extends String>) Constants.groups.get(key));
            for (String name : aux) {
                Group group = new Group(name, key);
                groupsList.add(group);

                if (actualUser.get("faculty").equals(key)) {
                    groupsList.add(new Group(name, key));
                }

                for (Faculty faculty : facultiesList) {
                    if (faculty.getRef().equals(key)) {
                        faculty.addGroup(group);
                        break;
                    }
                }
            }
        }

        HelpMethods.newsSorter(Constants.newsList);
        getSuggestions();

    }

    public static void getSuggestions() {

        String userFac = actualUser.get("faculty").toString();
        db.collection("faculties").document(userFac).collection("groups").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if (!task.getResult().isEmpty()) {
                                for (DocumentSnapshot documentSnapshot : task.getResult().getDocuments()) {
                                    if (documentSnapshot.getData().containsKey("hearts") && documentSnapshot.getData().containsKey("name")) {
                                        String roomName = documentSnapshot.getData().get("name").toString();

                                        db.collection("users").document(user.getEmail()).collection("loved_groups")
                                                .document(actualUser.get("faculty").toString() + "_" + roomName).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                if (task.isSuccessful()) {
                                                    if (!task.getResult().exists()) {
                                                        groupsPriority.put(documentSnapshot.getId(), Integer.valueOf(documentSnapshot.getData().get("hearts").toString()));
                                                    }
                                                }
                                            }
                                        });
                                    }
                                }
                                groupsPriority = HelpMethods.sortByComparator(groupsPriority, false);
                            }
                        }
                    }
                });

        db.collection("faculties").document(userFac).collection("teachers").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if (!task.getResult().isEmpty()) {
                                for (DocumentSnapshot documentSnapshot : task.getResult().getDocuments()) {
                                    if (documentSnapshot.getData().containsKey("hearts") && documentSnapshot.getData().containsKey("name")) {
                                        String roomName = documentSnapshot.getData().get("name").toString().replace(' ', '_');
                                        db.collection("users").document(user.getEmail()).collection("loved_teachers")
                                                .document(actualUser.get("faculty").toString() + "_" + roomName).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                if (task.isSuccessful()) {
                                                    if (!task.getResult().exists()) {
                                                        teachersPriority.put(documentSnapshot.getId(), Integer.valueOf(documentSnapshot.getData().get("hearts").toString()));
                                                    }
                                                }
                                            }
                                        });
                                    }
                                }
                                teachersPriority = HelpMethods.sortByComparator(teachersPriority, false);
                            }
                        }
                    }
                });

        db.collection("faculties").document(userFac).collection("classes").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if (!task.getResult().isEmpty()) {
                                for (DocumentSnapshot documentSnapshot : task.getResult().getDocuments()) {
                                    if (documentSnapshot.getData().containsKey("hearts") && documentSnapshot.getData().containsKey("name")) {
                                        String roomName = documentSnapshot.getData().get("name").toString().replace(' ', '_');
                                        db.collection("users").document(user.getEmail()).collection("loved_classes")
                                                .document(actualUser.get("faculty").toString() + "_" + roomName).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                if (task.isSuccessful()) {
                                                    if (!task.getResult().exists()) {
                                                        classesPriority.put(documentSnapshot.getId(), Integer.valueOf(documentSnapshot.getData().get("hearts").toString()));
                                                    }
                                                }
                                            }
                                        });
                                    }
                                }
                                classesPriority = HelpMethods.sortByComparator(classesPriority, false);
                            }
                        }
                    }
                });

        db.collection("faculties").document(userFac).collection("rooms").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if (!task.getResult().isEmpty()) {
                                for (DocumentSnapshot documentSnapshot : task.getResult().getDocuments()) {
                                    if (documentSnapshot.getData().containsKey("hearts") && documentSnapshot.getData().containsKey("name")) {
                                        String roomName = documentSnapshot.getData().get("name").toString();
                                        db.collection("users").document(user.getEmail()).collection("loved_rooms")
                                                .document(actualUser.get("faculty").toString() + "_" + roomName).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                if (task.isSuccessful()) {
                                                    if (!task.getResult().exists()) {
                                                        Log.d("MUIE", actualUser.get("faculty").toString() + "_" + roomName);
                                                        roomsPriority.put(documentSnapshot.getId(), Integer.valueOf(documentSnapshot.getData().get("hearts").toString()));
                                                    }
                                                }
                                            }
                                        });
                                    }
                                }
                                roomsPriority = HelpMethods.sortByComparator(roomsPriority, false);
                            }
                        }
                    }
                });


    }

    private static boolean isValidNews(Map<String, Object> map) {
        return map.containsKey("name") && map.containsKey("description") && map.containsKey("readMore") &&
                map.containsKey("linkOfNews") && map.containsKey("photoLink") && map.containsKey("importance");
    }

    public static void addNews(String key, Map<String, Object> map) {
        if (!news.containsKey(key) && map != null && isValidNews(map)) {
            news.put(key, map);
            newsList.add(new News(map.get("name").toString(),
                    map.get("description").toString(),
                    map.get("readMore").toString(),
                    map.get("linkOfNews").toString(),
                    map.get("photoLink").toString(), null, ((Long) map.get("importance")).intValue()));
        }
    }

    public static void addFaculty(String key, Map<String, Object> map) {
        faculties.put(key, map);
        facultiesStrings.put(key, map.get("name").toString());


        if (map.containsKey("teachers"))
            teachers.put(key, map.get("teachers"));

        if (map.containsKey("groups"))
            groups.put(key, map.get("groups"));

        if (map.containsKey("rooms"))
            rooms.put(key, map.get("rooms"));

        if (map.containsKey("classes"))
            classes.put(key, map.get("classes"));

    }


}
