package com.cdc.irimia.uaic_ez.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cdc.irimia.uaic_ez.Activities.LoginActivity;
import com.cdc.irimia.uaic_ez.Common.Constants;
import com.cdc.irimia.uaic_ez.Common.HelpMethods;
import com.cdc.irimia.uaic_ez.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class CameraFragment extends Fragment {

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private TextView text_initiale;
    private TextView text_userName;
    private TextView text_group;
    private TextView text_faculty;
    private Button logout;
    private Button settings;
    private Spinner spnText_changeGroup_Group;

    private Button btn_goBack;
    private Button btn_changeName;
    private Button btn_changeGroup;
    private Button btn_changePassword;

    private Button btn_goBack_Name;
    private Button btn_goBack_Password;
    private Button btn_goBack_Group;

    private Button btn_changeName_Name;
    private Button btn_changePassword_Password;
    private Button btn_changeGroup_Group;

    private EditText etdText_changeName_Name;
    private EditText etdText_changePassword_Password_Old;
    private EditText etdText_changePassword_Password_New1;
    private EditText etdText_changePassword_Password_New2;

    private RelativeLayout profile_options;
    private RelativeLayout profile_changeSettings;
    private RelativeLayout profile_changeGroup;
    private RelativeLayout profile_changeName;
    private RelativeLayout profile_changePassword;

    private boolean settingsMode = false;

    private WebView webView;
    private View.OnClickListener settingShow = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            profile_options.setVisibility(View.GONE);
            profile_changeGroup.setVisibility(View.GONE);
            profile_changeName.setVisibility(View.GONE);
            profile_changePassword.setVisibility(View.GONE);
            profile_changeSettings.setVisibility(View.VISIBLE);
            settingsMode = true;
        }
    };

    public static Fragment newInstance() {
        CameraFragment fragment = new CameraFragment();
        return fragment;
    }

    public void initialize(View rootview) {

        text_initiale = rootview.findViewById(R.id.num_txt);
        text_userName = rootview.findViewById(R.id.user_name);
        text_group = rootview.findViewById(R.id.user_group);
        text_faculty = rootview.findViewById(R.id.user_faculty);
        logout = rootview.findViewById(R.id.btn_logout);
        settings = rootview.findViewById(R.id.btn_changeSettings);
        webView = rootview.findViewById(R.id.profileWeb);
        profile_options = rootview.findViewById(R.id.profile_options);
        profile_changeSettings = rootview.findViewById(R.id.profile_changeSettings);
        btn_goBack = rootview.findViewById(R.id.btn_goBack);
        btn_changeName = rootview.findViewById(R.id.btn_changeName);
        btn_changeGroup = rootview.findViewById(R.id.btn_changeGroup);
        btn_changePassword = rootview.findViewById(R.id.btn_changePassword);
        profile_changeGroup = rootview.findViewById(R.id.profile_changeGroup);
        profile_changeName = rootview.findViewById(R.id.profile_changeName);
        profile_changePassword = rootview.findViewById(R.id.profile_changePassword);
        btn_goBack_Name = rootview.findViewById(R.id.btn_goBack_Name);
        btn_goBack_Password = rootview.findViewById(R.id.btn_goBack_Password);
        btn_goBack_Group = rootview.findViewById(R.id.btn_goBack_Group);
        spnText_changeGroup_Group = rootview.findViewById(R.id.spnText_changeGroup_Group);

        btn_changeName_Name = rootview.findViewById(R.id.btn_changeName_Name);
        btn_changePassword_Password = rootview.findViewById(R.id.btn_changePassword_Password);
        btn_changeGroup_Group = rootview.findViewById(R.id.btn_changeGroup_Group);
        etdText_changeName_Name = rootview.findViewById(R.id.etdText_changeName_Name);
        etdText_changePassword_Password_Old = rootview.findViewById(R.id.etdText_changePassword_Password_Old);
        etdText_changePassword_Password_New1 = rootview.findViewById(R.id.etdText_changePassword_Password_New1);
        etdText_changePassword_Password_New2 = rootview.findViewById(R.id.etdText_changePassword_Password_New2);

        btn_goBack_Name.setOnClickListener(settingShow);
        btn_goBack_Password.setOnClickListener(settingShow);
        btn_goBack_Group.setOnClickListener(settingShow);

        profile_changeSettings.setVisibility(View.GONE);
        profile_changeGroup.setVisibility(View.GONE);
        profile_changeName.setVisibility(View.GONE);
        profile_changePassword.setVisibility(View.GONE);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container != null) {
            container.removeAllViews();
        }

        View rootview = inflater.inflate(R.layout.fragment_camera, container, false);
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        initialize(rootview);

        String userGroup = "null";
        if (Constants.actualUser.isEmpty()) {

        } else {

            userGroup = Constants.actualUser.get("group").toString();


            WebSettings webSettings = webView.getSettings();
            webView.getSettings().setBuiltInZoomControls(true);
            webView.getSettings().setDisplayZoomControls(false);
            webSettings.setJavaScriptEnabled(true);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setUseWideViewPort(true);


            db.collection("faculties").document(Constants.actualUser.get("faculty").toString()).collection("groups").document(userGroup).get().addOnCompleteListener(
                    new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if (task.isSuccessful()) {
                                if (task.getResult().exists()) {
                                    if (task.getResult().getData().containsKey("link")) {
                                        webView.loadUrl(task.getResult().getData().get("link").toString());
                                    }
                                }
                            }
                        }
                    }
            );


            List<String> groupNames = new ArrayList<String>();


            groupNames.add("group...");
            if (Constants.actualUser.containsKey("faculty") && Constants.groups.containsKey(Constants.actualUser.get("faculty"))) {
                groupNames.addAll((Collection<? extends String>) Constants.groups.get(Constants.actualUser.get("faculty")));
            }

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, groupNames);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spnText_changeGroup_Group.setAdapter(dataAdapter);


            String userName = Constants.actualUser.get("name").toString();
            String faculty = Constants.facultiesStrings.get(Constants.actualUser.get("faculty").toString());
            String[] parts = userName.split(" ");
            text_userName.setText(userName);
            text_group.setText(userGroup);
            text_faculty.setText(faculty);
            text_initiale.setText(parts[0].substring(0, 1) + parts[1].substring(0, 1));


            logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mAuth = FirebaseAuth.getInstance();
                    mAuth.signOut();
                    getActivity().finish();
                    startLoginActivity();
                }
            });

            settings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    profile_options.setVisibility(View.GONE);
                    profile_changeGroup.setVisibility(View.GONE);
                    profile_changeName.setVisibility(View.GONE);
                    profile_changePassword.setVisibility(View.GONE);
                    profile_changeSettings.setVisibility(View.VISIBLE);
                    settingsMode = true;
                }
            });

            btn_goBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    profile_options.setVisibility(View.VISIBLE);
                    profile_changeGroup.setVisibility(View.GONE);
                    profile_changeName.setVisibility(View.GONE);
                    profile_changePassword.setVisibility(View.GONE);
                    profile_changeSettings.setVisibility(View.GONE);
                    settingsMode = false;
                }
            });

            btn_changeName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    profile_options.setVisibility(View.GONE);
                    profile_changeGroup.setVisibility(View.GONE);
                    profile_changeName.setVisibility(View.VISIBLE);
                    profile_changePassword.setVisibility(View.GONE);
                    profile_changeSettings.setVisibility(View.GONE);
                }
            });

            btn_changeGroup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    profile_options.setVisibility(View.GONE);
                    profile_changeGroup.setVisibility(View.VISIBLE);
                    profile_changeName.setVisibility(View.GONE);
                    profile_changePassword.setVisibility(View.GONE);
                    profile_changeSettings.setVisibility(View.GONE);
                }
            });

            btn_changePassword.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    profile_options.setVisibility(View.GONE);
                    profile_changeGroup.setVisibility(View.GONE);
                    profile_changeName.setVisibility(View.GONE);
                    profile_changePassword.setVisibility(View.VISIBLE);
                    profile_changeSettings.setVisibility(View.GONE);
                }
            });

        /*

        private Button btn_changeName_Name;
        private Button btn_changePassword_Password;
        private Button btn_changeGroup_Group;

        private EditText etdText_changeName_Name;
        private EditText etdText_changePassword_Password_Old;
        private EditText etdText_changePassword_Password_New1;
        private EditText etdText_changePassword_Password_New2;

        */

            btn_changeName_Name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String name = etdText_changeName_Name.getText().toString();
                    HelpMethods.hideKeyboard(getActivity());
                    if (HelpMethods.nameIsValid(name)) {
                        db.collection("users").document(user.getEmail()).update("name", name).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    text_userName.setText(name);
                                    btn_goBack.performClick();
                                    Constants.actualUser.remove("name");
                                    Constants.actualUser.put("name", name);
                                    Toast.makeText(getActivity(), "Name changed succesfully.", Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(getActivity(), "There was an error. Please try again later.", Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                    } else {
                        Toast.makeText(getActivity(), "This is not a name, please insert a valid name.", Toast.LENGTH_LONG).show();
                    }
                }
            });

            btn_changeGroup_Group.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int option = spnText_changeGroup_Group.getSelectedItemPosition();
                    String group = groupNames.get(option);
                    if (option != 0) {
                        db.collection("users").document(user.getEmail()).update("group", group).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    text_group.setText(group);
                                    btn_goBack.performClick();
                                    Constants.actualUser.remove("group");
                                    Constants.actualUser.put("group", group);
                                    Toast.makeText(getActivity(), "Group changed succesfully.", Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(getActivity(), "There was an error. Please try again later.", Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                    } else {
                        Toast.makeText(getActivity(), "There was an error. Please try again later.", Toast.LENGTH_LONG).show();
                    }
                }
            });


            btn_changePassword_Password.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String pwOld, pwNew1, pwNew2;
                    pwOld = etdText_changePassword_Password_Old.getText().toString();
                    pwNew1 = etdText_changePassword_Password_New1.getText().toString();
                    pwNew2 = etdText_changePassword_Password_New2.getText().toString();

                    if (pwNew1.equals(pwNew2) && HelpMethods.passIsValid(pwNew1)) {

                        user = FirebaseAuth.getInstance().getCurrentUser();
                        final String email = user.getEmail();
                        AuthCredential credential = EmailAuthProvider.getCredential(email, pwOld);

                        user.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    user.updatePassword(pwNew1).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                HelpMethods.hideKeyboard(getActivity());
                                                btn_goBack.performClick();
                                                Toast.makeText(getActivity(), "Password changed succesfully.", Toast.LENGTH_LONG).show();
                                            } else {
                                                Toast.makeText(getActivity(), "There was an error. Pleasy try again later.", Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    });
                                } else {
                                    Toast.makeText(getActivity(), "Old password is not corect.", Toast.LENGTH_LONG).show();
                                }
                            }
                        });


                    } else {
                        Toast.makeText(getActivity(), "Passwords does not match or they are not valid.", Toast.LENGTH_LONG).show();
                    }
                }
            });

        }
        HelpMethods.showActionBar(this.getActivity());
        return rootview;
    }

    private void startLoginActivity() {
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

}