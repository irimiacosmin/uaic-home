package com.cdc.irimia.uaic_ez.Objects;

import java.io.Serializable;

public class User implements Serializable {

    private String email;
    private String username;
    private String password;
    private String name;
    private String faculty;
    private String year;
    private String group;

    public User() {
        this.email = "-";
        this.username = "-";
        this.password = "-";
        this.name = "-";
        this.faculty = "-";
        this.year = "-";
        this.group = "-";
    }

    public User(String email, String username, String password, String name, String faculty, String year, String group) {
        this.email = email;
        this.username = username;
        this.password = password;
        this.name = name;
        this.faculty = faculty;
        this.year = year;
        this.group = group;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", faculty='" + faculty + '\'' +
                ", year='" + year + '\'' +
                ", group='" + group + '\'' +
                '}';
    }
}
