package com.cdc.irimia.uaic_ez.Activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.cdc.irimia.uaic_ez.Common.Constants;
import com.cdc.irimia.uaic_ez.Common.HelpMethods;
import com.cdc.irimia.uaic_ez.Fragments.CameraFragment;
import com.cdc.irimia.uaic_ez.Fragments.DashboardFragment;
import com.cdc.irimia.uaic_ez.Fragments.EtherFragment;
import com.cdc.irimia.uaic_ez.Fragments.InstanceFragment;
import com.cdc.irimia.uaic_ez.Fragments.SearchFragment;
import com.cdc.irimia.uaic_ez.R;
import com.cdc.irimia.uaic_ez.StaticObjects.Faculty;
import com.cdc.irimia.uaic_ez.StaticObjects.Group;
import com.cdc.irimia.uaic_ez.StaticObjects.Room;
import com.cdc.irimia.uaic_ez.StaticObjects.Teacher;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


public class MainActivity extends AppCompatActivity {

    public ImageButton imageButton;
    private Fragment selectedFragment = null;
    private Context context;
    private BottomNavigationView navigation;
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()) {
                case R.id.navigation_home:
                    selectedFragment = CameraFragment.newInstance();

                    break;
                case R.id.navigation_dashboard:
                    selectedFragment = DashboardFragment.newInstance();
                    break;
                case R.id.navigation_notifications:
                    selectedFragment = EtherFragment.newInstance();
                    break;
            }

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment, selectedFragment);
            transaction.commit();

            return true;
        }
    };

    private void setNavBarAndStatusBarColors(Activity activity) {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.setNavigationBarColor(getResources().getColor(R.color.uaicBlue));
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(activity, R.color.uaicBlue));

        }
    }

    public void askForNet() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    while (true) {
                        try {
                            if (!HelpMethods.isInternetAv(context)) {
                                MainActivity.this.runOnUiThread(new Runnable() {
                                    public void run() {
                                        Toast.makeText(context, "Our app need an internet connection to work properly. Trying to reconnect ...", Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            Thread.sleep(500);
                        } catch (Exception e) {
                        }
                    }
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }


    private void startInstanceFragment(Object instance) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, InstanceFragment.newInstance(instance));
        transaction.commit();
    }

    private void startSearchFragment(EditText searchText, ImageButton imageButton, TextView title) {
        title.setVisibility(View.GONE);
        searchText.setVisibility(View.VISIBLE);
        searchText.getLayoutParams().width = (int) (303 * getApplicationContext().getResources().getDisplayMetrics().density + 0.5f);
        searchText.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(searchText, InputMethodManager.SHOW_IMPLICIT);


        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, SearchFragment.newInstance(searchText, imageButton));
        transaction.commit();
    }


    private void ocrInstanceCheck() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {

                    int whatIsIt = getIntent().getIntExtra("whatIsIt", 0);
                    String facultyRef = getIntent().getStringExtra("facultyRef");
                    String itemName = getIntent().getStringExtra("itemName");

                    if (whatIsIt > 0 && facultyRef != null && itemName != null) {
                        switch (whatIsIt) {
                            case 1: {
                                for (Faculty item : Constants.facultiesList) {
                                    if (item.getName().equals(itemName) && item.getRef().equals(facultyRef)) {
                                        startInstanceFragment(item);
                                        break;
                                    }
                                }
                                break;
                            }
                            case 2: {
                                for (Teacher item : Constants.teacherList) {
                                    if (item.getName().equals(itemName) && item.getRef().equals(facultyRef)) {
                                        startInstanceFragment(item);
                                        break;
                                    }
                                }
                                break;
                            }
                            case 3: {
                                for (Room item : Constants.roomList) {
                                    if (item.getName().equals(itemName) && item.getRef().equals(facultyRef)) {
                                        startInstanceFragment(item);
                                        break;
                                    }
                                }
                                break;
                            }
                            case 4: {
                                for (Group item : Constants.groupsList) {
                                    if (item.getName().equals(itemName) && item.getRef().equals(facultyRef)) {
                                        startInstanceFragment(item);
                                        break;
                                    }
                                }
                                break;
                            }

                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 10);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNavBarAndStatusBarColors(this);
        context = getApplicationContext();
        askForNet();

        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.layout_actionbar_main);
        View view = getSupportActionBar().getCustomView();

        final TextView title = findViewById(R.id.mainActivityTitle);
        final EditText searchText = findViewById(R.id.searchBarEditText);
        final boolean[] searchIsPressed = {false};
        final float scale = getApplicationContext().getResources().getDisplayMetrics().density;
        imageButton = view.findViewById(R.id.action_bar_search);
        navigation = findViewById(R.id.navigation);
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();

        boolean priv = getIntent().getBooleanExtra("private", false);

        if (user.getEmail().equals("cirimia@bitdefender.com")) {
            priv = true;
        }

        if (priv) {
            selectedFragment = SearchFragment.newInstance(searchText, imageButton);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment, selectedFragment);
            transaction.commit();
        }

        ocrInstanceCheck();

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (searchIsPressed[0]) {

                    title.setVisibility(View.VISIBLE);
                    searchText.setVisibility(View.INVISIBLE);
                    searchText.getLayoutParams().width = (int) (190 * scale + 0.5f);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(searchText.getWindowToken(), 0);
                    searchIsPressed[0] = false;

                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.fragment, selectedFragment);
                    transaction.commit();


                } else {
                    title.setVisibility(View.GONE);
                    searchText.setVisibility(View.VISIBLE);
                    searchText.getLayoutParams().width = (int) (303 * scale + 0.5f);
                    searchText.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(searchText, InputMethodManager.SHOW_IMPLICIT);
                    searchIsPressed[0] = true;


                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.fragment, SearchFragment.newInstance(searchText, imageButton));
                    transaction.commit();
                }
            }
        });


        ImageButton imageButton2 = view.findViewById(R.id.action_bar_camera);

        imageButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startCameraActivity();
            }
        });


        if (!priv) {
            navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
            navigation.setSelectedItemId(R.id.navigation_dashboard);
        } else {
            navigation.setSelectedItemId(R.id.navigation_dashboard);
            navigation.setVisibility(View.GONE);
        }
    }

    private void startCameraActivity() {
        Intent intent = new Intent(this, CameraActivity.class);
        startActivity(intent);
    }

}
