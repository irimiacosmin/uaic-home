package com.cdc.irimia.uaic_ez.StaticObjects;

public class BuildingContainer {

    private String name;
    private String shortName;
    private Double xCo;
    private Double yCo;

    public BuildingContainer(String name, String shortName, Double xCo, Double yCo) {
        this.name = name;
        this.shortName = shortName;
        this.xCo = xCo;
        this.yCo = yCo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public Double getxCo() {
        return xCo;
    }

    public void setxCo(Double xCo) {
        this.xCo = xCo;
    }

    public Double getyCo() {
        return yCo;
    }

    public void setyCo(Double yCo) {
        this.yCo = yCo;
    }
}
