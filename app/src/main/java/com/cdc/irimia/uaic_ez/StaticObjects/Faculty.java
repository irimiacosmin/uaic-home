package com.cdc.irimia.uaic_ez.StaticObjects;

import com.google.firebase.firestore.GeoPoint;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Faculty implements Serializable {

    private String description;
    private String phoneNo;
    private String location;
    private String photoLink;
    private String website;
    private String virtualTour;
    private List<Group> groups;
    private List<Room> rooms;
    private List<Teacher> teachers;
    private List<Clazz> classes;
    private String ref;
    private GeoPoint geoPoint;
    private String name;
    private String email;
    private boolean isLoved;
    private int hearts;

    public Faculty() {
    }

    public Faculty(String ref, String name) {
        this.ref = ref;
        this.name = name;
        groups = new ArrayList<>();
        rooms = new ArrayList<>();
        teachers = new ArrayList<>();
        classes = new ArrayList<>();
    }

    public boolean isLoved() {
        return isLoved;
    }

    public void setLoved(boolean loved) {
        isLoved = loved;
    }

    public String getVirtualTour() {
        return virtualTour;
    }

    public void setVirtualTour(String virtualTour) {
        this.virtualTour = virtualTour;
    }

    public String getPhotoLink() {
        return photoLink;
    }

    public void setPhotoLink(String photoLink) {
        this.photoLink = photoLink;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void addTeacher(Teacher teacher) {
        teachers.add(teacher);
    }

    public void addClazz(Clazz clazz) {
        classes.add(clazz);
    }

    public void addRoom(Room room) {
        rooms.add(room);
    }

    public void addGroup(Group group) {
        groups.add(group);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    public List<Teacher> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<Teacher> teachers) {
        this.teachers = teachers;
    }

    public int getHearts() {
        return hearts;
    }

    public void setHearts(int hearts) {
        this.hearts = hearts;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public GeoPoint getGeoPoint() {
        return geoPoint;
    }

    public void setGeoPoint(GeoPoint geoPoint) {
        this.geoPoint = geoPoint;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Faculty{" +
                "description='" + description + '\'' +
                ", phoneNo='" + phoneNo + '\'' +
                ", location='" + location + '\'' +
                ", photoLink='" + photoLink + '\'' +
                ", website='" + website + '\'' +
                ", virtualTour='" + virtualTour + '\'' +
                ", groups=" + groups +
                ", rooms=" + rooms +
                ", teachers=" + teachers +
                ", ref='" + ref + '\'' +
                ", geoPoint=" + geoPoint +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
