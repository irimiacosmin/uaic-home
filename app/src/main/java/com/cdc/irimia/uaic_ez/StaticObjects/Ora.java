package com.cdc.irimia.uaic_ez.StaticObjects;

import java.util.ArrayList;
import java.util.List;

public class Ora {

    private String de_la;
    private String pana_la;
    private String disciplina;
    private String tip;
    private String sala;
    private List<String> profesori;
    private List<String> studenti;


    public Ora() {
        profesori = new ArrayList<>();
        studenti = new ArrayList<>();
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

    public String getDe_la() {
        return de_la;
    }

    public void setDe_la(String de_la) {
        this.de_la = de_la;
    }

    public String getPana_la() {
        return pana_la;
    }

    public void setPana_la(String pana_la) {
        this.pana_la = pana_la;
    }

    public String getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(String disciplina) {
        this.disciplina = disciplina;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public List<String> getProfesori() {
        return profesori;
    }

    public void setProfesori(List<String> profesori) {
        this.profesori = profesori;
    }

    public List<String> getStudenti() {
        return studenti;
    }

    public void setStudenti(List<String> studenti) {
        this.studenti = studenti;
    }

    @Override
    public String toString() {
        return "Ora{" +
                "de_la='" + de_la + '\'' +
                ", pana_la='" + pana_la + '\'' +
                ", disciplina='" + disciplina + '\'' +
                ", tip='" + tip + '\'' +
                ", sala='" + sala + '\'' +
                ", profesori=" + profesori +
                ", studenti=" + studenti +
                '}';
    }
}
