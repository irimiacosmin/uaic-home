package com.cdc.irimia.uaic_ez.StaticObjects;

import java.util.Objects;

public class News {

    private String title;
    private String description;
    private String readMoreString;
    private String someLink;
    private String pictureLink;
    private Integer pictureRes;
    private Integer importance;
    private boolean isLoved;
    private boolean isDeleted;

    public News(String title, String description, String readMoreString, String someLink, String pictureLink, Integer pictureRes, Integer importance) {
        this.title = title;
        this.description = description;
        this.readMoreString = readMoreString;
        this.someLink = someLink;
        this.pictureLink = pictureLink;
        this.pictureRes = pictureRes;
        this.importance = importance;
    }

    public boolean isLoved() {
        return isLoved;
    }

    public void setLoved(boolean loved) {
        isLoved = loved;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getPictureLink() {
        return pictureLink;
    }

    public void setPictureLink(String pictureLink) {
        this.pictureLink = pictureLink;
    }

    public Integer getPictureRes() {
        return pictureRes;
    }

    public void setPictureRes(Integer pictureRes) {
        this.pictureRes = pictureRes;
    }

    public String getReadMoreString() {
        return readMoreString;
    }

    public void setReadMoreString(String readMoreString) {
        this.readMoreString = readMoreString;
    }

    public Integer getImportance() {
        return importance;
    }

    public void setImportance(Integer importance) {
        this.importance = importance;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSomeLink() {
        return someLink;
    }

    public void setSomeLink(String someLink) {
        this.someLink = someLink;
    }

    @Override
    public String toString() {
        return "News{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", readMoreString='" + readMoreString + '\'' +
                ", someLink='" + someLink + '\'' +
                ", pictureLink='" + pictureLink + '\'' +
                ", pictureRes=" + pictureRes +
                ", importance=" + importance +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        News news = (News) o;
        return
                Objects.equals(title, news.title) &&
                        Objects.equals(description, news.description) &&
                        Objects.equals(pictureLink, news.pictureLink) && Objects.equals(importance, news.importance);
    }

    @Override
    public int hashCode() {

        return Objects.hash(title, description, readMoreString, someLink, pictureLink, pictureRes, importance, isLoved, isDeleted);
    }
}
