package com.cdc.irimia.uaic_ez.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cdc.irimia.uaic_ez.Common.Constants;
import com.cdc.irimia.uaic_ez.Common.HelpMethods;
import com.cdc.irimia.uaic_ez.R;
import com.cdc.irimia.uaic_ez.StaticObjects.Building;
import com.cdc.irimia.uaic_ez.StaticObjects.Clazz;
import com.cdc.irimia.uaic_ez.StaticObjects.Faculty;
import com.cdc.irimia.uaic_ez.StaticObjects.Group;
import com.cdc.irimia.uaic_ez.StaticObjects.Room;
import com.cdc.irimia.uaic_ez.StaticObjects.Teacher;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.jcraft.jsch.HASH;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class HelpFragment extends Fragment {

    private View rootView;
    private Object instance;
    private HelpFragment fragment;

    private TextView name;
    private TextView faculty;
    private EditText email;
    private EditText phone;
    private EditText website;
    private EditText cabinet;
    private EditText floor;
    private EditText somethingElse;
    private CheckBox agreeShit;
    private Button sendButton;
    private boolean canSend;
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser user;

    public HelpFragment(Object instance) {
        this.instance = instance;
        this.fragment = this;
    }

    public static Fragment newInstance(Object instance) {
        return new HelpFragment(instance);
    }

    private void instantiateAll() {
        name = rootView.findViewById(R.id.help_title);
        faculty = rootView.findViewById(R.id.help_description);
        email = rootView.findViewById(R.id.help_email);
        phone = rootView.findViewById(R.id.help_phone);
        website = rootView.findViewById(R.id.help_website);
        cabinet = rootView.findViewById(R.id.help_cabinet);
        floor = rootView.findViewById(R.id.help_floor);
        somethingElse = rootView.findViewById(R.id.help_somethingElse);
        agreeShit = rootView.findViewById(R.id.help_checkBox);
        sendButton = rootView.findViewById(R.id.help_sendButton);
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();

    }

    public void showInformAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(rootView.getContext());
        builder.setTitle("We need your help...");
        builder.setMessage("Our application is in continuous development and we need help from you. Can you please help us to fill the missing informations?");
        builder.setPositiveButton("I can help!", null);
        builder.setNegativeButton("I want back!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startInstanceFragment();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        Button negativeButton = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        positiveButton.setTextColor(Color.parseColor("#00AFF0"));
        negativeButton.setTextColor(Color.parseColor("#FF0000"));

    }

    public void showThanksAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(rootView.getContext());
        builder.setTitle("You are awesome!");
        builder.setMessage("Thank you for helping us, we will reward you with new features and better app experience.");
        builder.setPositiveButton("You are welcome!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startInstanceFragment();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveButton.setTextColor(Color.parseColor("#00AFF0"));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void startInstanceFragment() {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, InstanceFragment.newInstance(instance));
        transaction.commit();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container != null) {
            container.removeAllViews();
        }
        HelpMethods.showActionBar(this.getActivity());
        View rootview = inflater.inflate(R.layout.fragment_help, container, false);
        this.rootView = rootview;
        instantiateAll();

        showInformAlert();

        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    startInstanceFragment();
                    return true;
                }
                return false;
            }
        });


        String className = instance.getClass().getName();

        switch (className) {
            case "com.cdc.irimia.uaic_ez.StaticObjects.Group": {
                treatGroup((Group) instance);
                break;
            }
            case "com.cdc.irimia.uaic_ez.StaticObjects.Room": {
                treatRoom((Room) instance);
                break;
            }
            case "com.cdc.irimia.uaic_ez.StaticObjects.Teacher": {
                treatTeacher((Teacher) instance);
                break;
            }
            case "com.cdc.irimia.uaic_ez.StaticObjects.Faculty": {
                treatFaculty((Faculty) instance);
                break;
            }
            case "com.cdc.irimia.uaic_ez.StaticObjects.Building": {
                treatBuilding((Building) instance);
                break;
            }
            case "com.cdc.irimia.uaic_ez.StaticObjects.Clazz": {
                treatClazz((Clazz) instance);
                break;
            }
        }


        return rootview;
    }

    private int addToMyMap(Map<String, String> map, String name, EditText myText) {
        if (name == null || myText.getText() == null || myText.getText().length() < 1)
            return -1;
        if (name.equals("email") && !HelpMethods.emailIsValid(myText.getText().toString())) {
            Toast.makeText(rootView.getContext(), "Email is not valid. Please try again.", Toast.LENGTH_LONG).show();
            canSend = false;
            return 0;
        }
        if (name.equals("phoneNo") && (myText.getText().length() < 10 || myText.getText().length() > 15)) {
            Toast.makeText(rootView.getContext(), "Phone number is not valid. Please try again.", Toast.LENGTH_LONG).show();
            canSend = false;
            return 0;
        }
        if (name.equals("floor") && (Integer.valueOf(myText.getText().toString()) < -2 || Integer.valueOf(myText.getText().toString()) > 9)) {
            Toast.makeText(rootView.getContext(), "Floor number is not valid. Please try again.", Toast.LENGTH_LONG).show();
            canSend = false;
            return 0;
        }
        if (name.equals("room") && (myText.getText().toString().length() < 2 || myText.getText().toString().length() > 20)) {
            Toast.makeText(rootView.getContext(), "Cabinet room is not valid. Please try again.", Toast.LENGTH_LONG).show();
            canSend = false;
            return 0;
        }

        if (name.equals("website") && !HelpMethods.websiteIsValid(myText.getText().toString())) {
            Toast.makeText(rootView.getContext(), "Website is not valid. Please try again.", Toast.LENGTH_LONG).show();
            canSend = false;
            return 0;
        }

        if (name.equals("website") && !HelpMethods.websiteIsValid(myText.getText().toString())) {
            Toast.makeText(rootView.getContext(), "Website is not valid. Please try again.", Toast.LENGTH_LONG).show();
            canSend = false;
            return 0;
        }

        map.put(name, myText.getText().toString());
        return 1;

    }


    public void globalShow(String ref, String objTypeFamily, String objName, boolean needsPhoneNo,
                           boolean needsEmail, boolean needsFloor, boolean needsWebsite,
                           boolean needsCabinet, boolean needsSomethingElse) {

        if (ref == null || objName == null || objTypeFamily == null || !Constants.facultiesStrings.containsKey(ref)) {
            sendButton.setVisibility(View.GONE);
            agreeShit.setVisibility(View.GONE);
            this.name.setText("There was an error");
            this.faculty.setText("Please try again later");
            return;
        }

        this.name.setText(objName);
        this.faculty.setText(Constants.facultiesStrings.get(ref));

        if (needsPhoneNo) phone.setVisibility(View.VISIBLE);
        if (needsEmail) email.setVisibility(View.VISIBLE);
        if (needsFloor) floor.setVisibility(View.VISIBLE);
        if (needsWebsite) website.setVisibility(View.VISIBLE);
        if (needsCabinet) cabinet.setVisibility(View.VISIBLE);
        if (needsSomethingElse) somethingElse.setVisibility(View.VISIBLE);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (agreeShit.isChecked()) {
                    canSend = false;
                    Map<String, String> info = new HashMap<>();
                    canSend = true;
                    int sumOfDecisions = addToMyMap(info, "phoneNo", phone) +
                            addToMyMap(info, "email", email) +
                            addToMyMap(info, "room", cabinet) +
                            addToMyMap(info, "floor", floor) +
                            addToMyMap(info, "website", website) +
                            addToMyMap(info, "somethingElse", somethingElse);

                    if (sumOfDecisions == -6) {
                        Toast.makeText(rootView.getContext(), "You must provide some info before pressing the button.", Toast.LENGTH_LONG).show();
                    } else if (canSend) {
                        showThanksAlert();
                        Map<String, Object> aux = new HashMap<>();
                        final boolean[] sameAgain = {false};
                        aux.put(new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()).toString(), info);
                        if (user.getEmail() != null)
                            db.collection("community").document(ref).collection(objTypeFamily).document(objName)
                                    .collection("users").document(user.getEmail()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    if (task.isSuccessful() && task.getResult().exists()) {
                                        aux.putAll(task.getResult().getData());
                                        sameAgain[0] = true;
                                    }
                                    db.collection("community").document(ref).collection(objTypeFamily).document(objName)
                                            .collection("users").document(user.getEmail()).set(aux);
                                    Map<String,Object> inutil = new HashMap<String, Object>();
                                    inutil.put("name",objName);
                                    db.collection("community").document(ref).collection(objTypeFamily).document(objName).set(inutil);

                                }
                            });



                            db.collection("users").document(user.getEmail()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    if(task.isSuccessful() && !task.getResult().getData().isEmpty()){
                                        Map<String, Object> aux = task.getResult().getData();
                                        int rep = 0;
                                        if(aux.containsKey("reputation")){
                                            try{
                                                rep = Integer.valueOf(aux.get("reputation").toString());

                                                if(sameAgain[0]){
                                                    rep = rep - 9;
                                                }else{
                                                    rep = rep + (10 * info.size());
                                                }

                                                if(rep < 0){
                                                    InstanceFragment.canSendData = false;
                                                }
                                                aux.put("reputation",String.valueOf(rep));
                                                db.collection("users").document(user.getEmail()).set(aux);
                                            }
                                            catch (Exception e){}
                                        }else{
                                            aux.put("reputation", 10 * info.size());
                                            db.collection("users").document(user.getEmail()).set(aux);
                                        }
                                    }
                                }
                            });

                        sendButton.setEnabled(false);
                        sendButton.setTextColor(Color.GRAY);
                    }

                } else {
                    Toast.makeText(rootView.getContext(), "Please agree that the provided info is valid", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    public void treatGroup(Group obj) {
        globalShow(obj.getRef(), "groups", obj.getName(), false, false, false, false, false, true);
    }

    public void treatRoom(Room obj) {
        globalShow(obj.getRef(), "rooms", obj.getName(), obj.getPhoneNo() == null, obj.getEmail() == null, obj.getFloor() == null, false, false, true);

    }

    public void treatTeacher(Teacher obj) {
        globalShow(obj.getRef(), "teachers", obj.getName(), obj.getPhoneNo() == null, obj.getEmail() == null, false, obj.getWebsite() == null, obj.getRoom() == null, true);
    }

    public void treatFaculty(Faculty obj) {
        globalShow(obj.getRef(), "faculties", obj.getName(), obj.getPhoneNo() == null, obj.getEmail() == null, false, obj.getWebsite() == null, false, true);

    }

    public void treatBuilding(Building obj) {

    }

    public void treatClazz(Clazz obj) {
        globalShow(obj.getRef(), "courses", obj.getName(), false, false, false, false, false, true);
    }

}
