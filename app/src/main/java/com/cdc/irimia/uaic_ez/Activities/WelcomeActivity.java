package com.cdc.irimia.uaic_ez.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cdc.irimia.uaic_ez.Common.Constants;
import com.cdc.irimia.uaic_ez.Common.HelpMethods;
import com.cdc.irimia.uaic_ez.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.QuerySnapshot;

public class WelcomeActivity extends AppCompatActivity {

    private Context context;

    private void setNavBarAndStatusBarColors(Activity activity) {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.setNavigationBarColor(getResources().getColor(R.color.uaicBlue));
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(activity, R.color.uaicBlue));
        }
    }

    private void startLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        this.finish();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        setNavBarAndStatusBarColors(this);
        context = this.getApplicationContext();
        downloadAllThings();
    }

    private void downloadAllThings() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    try {
                        TextView textView = findViewById(R.id.textView);
                        ProgressBar progressBar = findViewById(R.id.progressBar);
                        if (!HelpMethods.isInternetAv(context)) {
                            textView.setText("It seems like you don't have internet acces.\n Trying to reconnect ...");
                            progressBar.setVisibility(View.INVISIBLE);
                            do {
                                Thread.sleep(500);
                            } while (!HelpMethods.isInternetAv(context));
                            FirebaseFirestore db = FirebaseFirestore.getInstance();
                            FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                                    .setTimestampsInSnapshotsEnabled(true)
                                    .build();
                            db.setFirestoreSettings(settings);
                            downloadFaculties(db);
                            textView.setText("Please wait ...");
                        } else {
                            textView.setText("Please wait ...");
                            progressBar.setVisibility(View.VISIBLE);
                            FirebaseFirestore db = FirebaseFirestore.getInstance();
                            FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                                    .setTimestampsInSnapshotsEnabled(true)
                                    .build();
                            db.setFirestoreSettings(settings);
                            downloadFaculties(db);
                        }
                    } catch (Exception e) {
                    }
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    private void downloadFaculties(FirebaseFirestore db) {
        if (db.collection("faculties")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful() && !task.getResult().isEmpty()) {
                            for (DocumentSnapshot document : task.getResult()) {
                                Constants.addFaculty(document.getId(), document.getData());

                            }
                            startLoginActivity();
                        }
                    }
                }).isComplete()) {
        }
    }

}
