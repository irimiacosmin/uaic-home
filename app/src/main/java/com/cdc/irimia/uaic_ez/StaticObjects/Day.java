package com.cdc.irimia.uaic_ez.StaticObjects;

import java.util.ArrayList;
import java.util.List;

public class Day {

    private String name;
    private String type = null;
    private List<Ora> oraList;

    public Day(String name, List<Ora> oraList) {
        this.name = name;
        this.oraList = oraList;
    }

    public Day(String name) {
        this.name = name;
        this.oraList = new ArrayList<>();
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void addOra(Ora ora) {
        oraList.add(ora);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Ora> getOraList() {
        return oraList;
    }

    public void setOraList(List<Ora> oraList) {
        this.oraList = oraList;
    }

    @Override
    public String toString() {
        return "Day{" +
                "name='" + name + '\'' +
                ", oraList=" + oraList +
                '}';
    }
}
